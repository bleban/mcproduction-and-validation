import numpy as np


sm_pars = {
    'GF': 1.1663787000000001e-05,
    'alpha_e': 0.00781616,
    'alpha_s': 0.1182,
    'm_c': 1.273,
    'm_b': 4.195,
    'm_s': 0.09247,
    'm_d': 0.004675,
    'm_u': 0.00213,
    'm_W': 80.379,
    'm_Z': 91.1876,
    'm_e': 0.0005109989461,
    'm_mu': 0.1056583745,
    'm_tau': 1.77686,
    'm_t': 173.1,
    'm_H': 125.1}

def_masses = {'m_WR': 6000, 'm_DRPP': 500, 'm_DL0': 500,
              'm_DD': 500, 'm_HH': 20000, 'm_AA': 20100}

def_pars = {**def_masses, 'tb': 0.1, 'zetaLR': 1,
            'theta': 0.01, 'eta': 1.e-5, 'phi': 0., 'alp': 0., 'r4': 0}


def parse_param_card(param_card_path):
    """
    Parses the parameter card file to extract masses and LRSM inputs.

    Args:
    param_card_path (str): The file path to the parameter card.

    Returns:
    dict: A dictionary containing particle masses.
    dict: A dictionary containing LRSM inputs.
    """
    masses = {}
    lrsm_inputs = {}

    with open(param_card_path, 'r') as param_card:
        block = None
        for line in param_card:
            line = line.strip()
            if line.startswith('Block mass'):
                block = 'mass'
            elif line.startswith('Block lrsminputs'):
                block = 'lrsminputs'
            elif line.startswith('Block pmnsblock'):
                block = None
            elif block == 'mass' and line and not line.startswith('#'):
                parts = line.split()
                if len(parts) == 4:
                    try:
                        particle_id = str(parts[3])
                        mass_value = float(parts[1])
                        masses[particle_id] = mass_value
                    except (ValueError, IndexError):
                        pass
            elif block == 'lrsminputs' and line and not line.startswith('#'):
                parts = line.split()
                if len(parts) == 4:
                    try:
                        lrsm_input_id = str(parts[3])
                        lrsm_input_value = float(parts[1])
                        lrsm_inputs[lrsm_input_id] = lrsm_input_value
                    except (ValueError, IndexError):
                        pass

    return masses, lrsm_inputs


def epar_dict_from_param_card(param_card_path):
    """
    Creates a dictionary of external parameters from a parameter card.

    Args:
    param_card_path (str): The file path to the parameter card.

    Returns:
    dict: A dictionary containing the translated external parameters, which include masses and LRSM inputs.
    """
    mg5_translate = {'mHH': 'm_HH',
                     'mDD': 'm_DD',
                     'mAA': 'm_AA',
                     'MWR': 'm_WR',
                     'mDRpp': 'm_DRPP',
                     'mDL0': 'm_DL0',
                     'tb': 'tb',
                     'alp': 'alp',
                     'thetamix': 'theta',
                     'etamix': 'eta',
                     'phimix': 'phi',
                     'r4': 'r4',
                     'zetaLR': 'zetaLR'
                     }
    masses, lrsm_inputs = parse_param_card(param_card_path)
    param_card_dict = {**masses, **lrsm_inputs}
    epar = {mg5_translate[k]: param_card_dict[k] for k in mg5_translate.keys()}
    return epar


def calc_internal_params(epar_dict):
    """
    Calculates internal parameters based on the parameters provided in epar_dict.
    Args:
    epar_dict (dict): A dictionary containing the parameters required for the calculations.
    Returns:
    dict: A dictionary containing the calculated internal parameters.
    """
    pars_ = {**sm_pars, **epar_dict}
    ipars = {}
    vev = 1 / np.sqrt(np.sqrt(2) * pars_['GF'])
    ipars['vev'] = vev
    MW = pars_['m_W']
    MZ = pars_['m_Z']

    sw2 = 1 - MW**2 / MZ**2
    ipars['sw2'] = sw2
    sw = np.sqrt(sw2)
    cw = MW / MZ
    tw = sw / cw
    ipars['sw'] = np.sqrt(sw2)
    ipars['cw'] = np.sqrt(1 - sw2)
    ipars['tw'] = ipars['sw'] / ipars['cw']

    gw = np.sqrt(8 * pars_['GF'] * MW**2 / np.sqrt(2))
    ipars['gw'] = gw
    ipars['gL'] = ipars['gw']

    MWR = pars_['m_WR']
    zLR = pars_['zetaLR']

    vR = np.sqrt(MWR**2 - MW**2 * zLR**2) / (gw * zLR)
    ipars['vR'] = vR

    ep = vev / vR
    ipars['ep'] = ep

    rr = tw / np.sqrt(zLR**2 - tw**2)
    ipars['rr'] = rr

    gBL = gw * rr * zLR
    ipars['gBL'] = gBL

    ipars['gR'] = pars_['zetaLR'] * ipars['gL']

    tb = pars_['tb']
    beta = np.arctan(tb)
    ipars['beta'] = beta

    sb = np.sin(beta)
    ipars['sb'] = sb

    cb = np.cos(beta)
    ipars['cb'] = cb

    c2b = np.cos(2 * beta)
    ipars['c2b'] = c2b

    s2b = np.sin(2 * beta)
    ipars['s2b'] = s2b

    xi = np.arcsin(ep**2 * cb * sb / zLR)
    ipars['xi'] = xi

    sxi = np.sin(xi)
    ipars['sxi'] = sxi

    cxi = np.cos(xi)
    ipars['cxi'] = cxi

    v2 = vev * sb
    ipars['v2'] = v2

    v1 = vev * cb
    ipars['v1'] = v1

    mh = pars_['m_H']
    MAA = pars_['m_AA']
    MHH = pars_['m_HH']
    MDD = pars_['m_DD']
    MDRPP = pars_['m_DRPP']
    MDL0 = pars_['m_DL0']
    eta = pars_['eta']
    theta = pars_['theta']
    phi = pars_['phi']
    alp = pars_['alp']

    eta2 = ((-MDD**2 + MHH**2) * (1 + tb**2)**2 * np.sin(2 * eta) / (4 * ep * MAA**2 * np.sqrt(1 + tb**4 - 2 * tb**2 * np.cos(2 * alp))))
    ipars['eta2'] = eta2

    X = MAA**2 / vR**2
    Y = (MDD**2 + (-MDD**2 + MHH**2) * np.sin(eta)**2) / ((1 + theta**2) * vR**2)
    ipars['X'] = X
    ipars['Y'] = Y

    a3 = -(((-1 + (tb)**2) * X) / (1 + tb**2))
    ipars['a3'] = a3

    r1 = Y / 4
    ipars['r1'] = r1

    d2 = np.arctan(((tb + 1e-15) * np.sin(alp)) / (eta2 - (tb + 1e-15) * np.cos(alp)))
    ipars['d2'] = d2

    l2 = -1 / 16 * ((1 + tb**2)**2 * (MAA**2 - MHH**2 + (-MDD**2 + MHH**2) * np.sin(eta)**2)) / (ep**2 * vR**2 * (1 + tb)**4 - 2 * tb**2 * np.cos(2 * alp))
    ipars['l2'] = l2

    l3 = 2 * l2
    ipars['l3'] = l3

    l4 = (eta2 * MAA**2 * theta) / (2 * ep * (1 + tb**2) * vR**2) + ((MAA**2 * phi * (1 + tb**2)) / (4 * vR**2 * (-1 + tb**2 * np.cos(2 * alp))) + (tb * (1 + tb**2) * np.cos(alp) * (2 * MAA**2 - MDD**2 - MHH**2 + (MDD**2 - MHH**2) * np.cos(2 * eta))) / (4 * vR**2 * (1 + tb**4 - 2 * tb**2 * np.cos(2 * alp)))) / ep**2
    ipars['l4'] = l4

    l1 = mh**2 / (4 * ep**2 * vR**2) + (theta**2 * Y) / (4 * ep**2) - (4 * l4 * tb * np.cos(alp)) / (1 + tb**2) - (16 * l2 * tb**2 * np.cos(alp)**2) / (1 + tb**2)**2
    ipars['l1'] = l1

    a1 = (theta * Y) / (2 * ep) + (tb * X * (tb + tb**3 - 2 * eta2 * np.cos(alp))) / (1 + tb**2)**2 + ep**2 * ((-2 * l1 * theta) / ep - (8 * l4 * tb * theta * np.cos(alp)) / (ep * (1 + tb**2)) - (32 * l2 * tb**2 * theta * np.cos(alp)**2) / (ep * (1 + tb**2)**2))
    ipars['a1'] = a1

    a2 = (X * (eta2 - tb * np.cos(alp)) * np.sqrt(1 + ((tb + 1e-15)**2 * np.sin(alp)**2) / (eta2 - (tb + 1e-15) * np.cos(alp))**2)) / (2 * (1 + tb**2))
    ipars['a2'] = a2

    r2 = (MDRPP**2 / vR**2 - (ep**2 * (-1 + tb**2)**2 * X) / (1 + tb**2)**2)/4
    ipars['r2'] = r2

    r3 = MDL0**2 / vR**2 + Y / 2 - (4 * ep**2 * tb**2 * X * np.sin(alp)**2) / (1 + tb**2)**2
    ipars['r3'] = r3

    mu1 = -((-2 * l1 * v1**4 + 2 * l1 * v2**4 - a1 * v1**2 * vR**2 + a1 * v2**2 * vR**2 + a3 * v2**2 * vR**2 - \
            4 * l4 * v1**3 * v2 * np.cos(alp) + 4 * l4 * v1 * v2**3 * np.cos(alp)) / (v1**2 - v2**2))
    ipars['mu1'] = mu1

    mu2 = -1 / 2 * ((-4 * l3 * v1**3 * v2 + 4 * l3 * v1 * v2**3 - a3 * v1 * v2 * vR**2 - 2 * l4 * v1**4 * np.cos(alp) + 2 * l4 * v2**4 * np.cos(alp) - 8 * l2 * v1**3 * v2 * np.cos(2 * alp) + 8 * l2 * v1 * v2**3 * np.cos(2 * alp) - 2 * a2 * v1**2 * vR**2 * np.cos(alp + d2) + 2 * a2 * v2**2 * vR**2 * np.cos(alp + d2)) / np.cos(alp)) / (v1**2 - v2**2)
    ipars['mu2'] = mu2

    mu3 = a1 * v1**2 + a1 * v2**2 + a3 * v2**2 + 2 * r1 * \
        vR**2 + 4 * a2 * v1 * v2 * np.cos(alp + d2)
    ipars['mu3'] = mu3

    return ipars


def calc_internal_masses(epar_dict):
    """
    Calculates internal masses based on the parameters provided in epar_dict.
    Args:
    epar_dict (dict): A dictionary containing the parameters required for the calculations.
    Returns:
    dict: A dictionary containing the calculated internal masses.
    """
    ipars = calc_internal_params(epar_dict)
    par_dict = {**ipars, **epar_dict, **sm_pars}
    imasses = {}

    MW = par_dict['m_W']
    MZ = par_dict['m_Z']
    zLR = par_dict['zetaLR']
    MWR = par_dict['m_WR']
    ep = par_dict['ep']

    MZR = (ep**2 * MWR) / (4 * np.sqrt(2) * MW**3 * zLR**2 * ((-MZ**2 + MW**2 * (1 + zLR**2))**(-1))**(3 / 2)) + np.sqrt(2) * MW * MWR * zLR**2 * np.sqrt((-MZ**2 + MW**2 * (1 + zLR**2))**(-1))
    imasses['m_ZR'] = MZR

    MDL0 = par_dict['m_DL0']
    tb = par_dict['tb']
    vR = par_dict['vR']
    X = par_dict['X']

    MDLPP = MDL0 + (ep**2 * (-1 + tb**2)**2 * vR**2 * X) / (2 * MDL0 * (1 + tb**2)**2)
    imasses['m_DLPP'] = MDLPP

    MDLP = MDL0 + (ep**2 * (-1 + tb**2)**2 * vR**2 * X) / (4 * MDL0 * (1 + tb**2)**2)
    imasses['m_DLP'] = MDLP

    imasses['m_ChiL0'] = MDL0

    beta = par_dict['beta']
    MHP = (vR * np.sqrt(X * (4 + ep**2 + ep**2 * np.cos(4 * beta)))) / 2
    imasses['m_HP'] = MHP

    return imasses


def get_all_masses(epar_dict):
    """
    Retrieves all particle masses by combining externally provided masses with internally calculated ones.

    Args:
    epar_dict (dict): A dictionary containing the external parameters and masses required for the calculations.

    Returns:
    dict: A dictionary of all particle masses, including both external and internal masses.
    """
    emasses = {m: epar_dict[m] for m in def_masses}
    imasses = calc_internal_masses(epar_dict)

    return {**emasses, **imasses}


def get_all_params(epar_dict):
    """
    Retrieves a merged dictionary of Standard Model parameters, external parameters, and internally calculated parameters.

    Args:
    epar_dict (dict): A dictionary containing the external parameters required for calculations.

    Returns:
    dict: A merged dictionary of parameters including Standard Model, external, and internal parameters.
    """
    ipars = calc_internal_params(epar_dict)
    imasses = calc_internal_masses(epar_dict)

    return {**sm_pars, **epar_dict, **ipars, **imasses}


def get_quartics(epar_dict):
    """
    Extracts quartic coupling parameters from a parameter dictionary.

    This function retrieves all quartic coupling parameters (a1, a2, a3, r1, r2, r3, r4, l1, l2, l3, l4)
    from the merged parameter dictionary obtained by calling `get_all_params` with `epar_dict`.
    If a parameter is not found, it defaults to 0.

    Args:
    epar_dict (dict): A dictionary containing the external parameters.

    Returns:
    dict: A dictionary with keys as quartic coupling parameters and their corresponding values.
          Missing parameters are set to 0.
    """
    ipars = get_all_params(epar_dict)
    outdict = {}

    for p in ['a1', 'a2', 'a3', 'r1', 'r2', 'r3', 'r4', 'l1', 'l2', 'l3', 'l4']:
        try:
            outdict[p] = ipars[p]
        except:
            outdict[p] = 0

    return outdict
