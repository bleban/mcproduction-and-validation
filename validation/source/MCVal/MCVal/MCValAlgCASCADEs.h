#ifndef MCVal_MCValAlgCASCADEs_H
#define MCVal_MCValAlgCASCADEs_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <MCTruthClassifier/MCTruthClassifier.h>

#include <TH1.h>
#include <TH2.h>
#include <TTree.h>

#include "MCVal/MCValUtils.h"

class MCValAlgCASCADEs : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MCValAlgCASCADEs(const std::string &name, ISvcLocator *pSvcLocator);

  ~MCValAlgCASCADEs() override;

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

  // Algorithm properties
  int m_pdgIdD0;
  int m_pdgIdDP;
  int m_pdgIdDPP;
  int m_pdgIdChi;
  bool m_detector;

  // Event properties to save
  unsigned int m_runNumber = 0;         ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number

  float m_met;

  std::vector<float> *m_d0_pt;
  std::vector<float> *m_d0_eta;
  std::vector<float> *m_d0_phi;
  std::vector<float> *m_d0_m;
  std::vector<int> *m_d0_pdgid;

  std::vector<float> *m_dp_pt;
  std::vector<float> *m_dp_eta;
  std::vector<float> *m_dp_phi;
  std::vector<float> *m_dp_m;
  std::vector<int> *m_dp_pdgid;

  std::vector<float> *m_dpp_pt;
  std::vector<float> *m_dpp_eta;
  std::vector<float> *m_dpp_phi;
  std::vector<float> *m_dpp_m;
  std::vector<int> *m_dpp_pdgid;

  std::vector<float> *m_chi_pt;
  std::vector<float> *m_chi_eta;
  std::vector<float> *m_chi_phi;
  std::vector<float> *m_chi_m;
  std::vector<int> *m_chi_pdgid;

  std::vector<float> *m_lep_pt;
  std::vector<float> *m_lep_eta;
  std::vector<float> *m_lep_phi;
  std::vector<float> *m_lep_m;
  std::vector<int> *m_lep_pdgid;

  std::vector<float> *m_el_pt;
  std::vector<float> *m_el_eta;
  std::vector<float> *m_el_phi;
  std::vector<float> *m_el_m;

  std::vector<float> *m_mu_pt;
  std::vector<float> *m_mu_eta;
  std::vector<float> *m_mu_phi;
  std::vector<float> *m_mu_m;

  std::vector<float> *m_tau_pt;
  std::vector<float> *m_tau_eta;
  std::vector<float> *m_tau_phi;
  std::vector<float> *m_tau_m;

  std::vector<float> *m_q_pt;
  std::vector<float> *m_q_eta;
  std::vector<float> *m_q_phi;
  std::vector<float> *m_q_m;
  std::vector<int> *m_q_pdgid;

  std::vector<float> *m_jet_pt;
  std::vector<float> *m_jet_eta;
  std::vector<float> *m_jet_phi;
  std::vector<float> *m_jet_m;

private:
  // Configuration, and any other types of variables go here.
  // float m_cutValue;
  // TTree *m_myTree;
  // TH1 *m_myHist;
  std::string m_format;
};

#endif
