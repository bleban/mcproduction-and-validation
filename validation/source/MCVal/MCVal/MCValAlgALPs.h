#ifndef MCVal_MCValAlgALPs_H
#define MCVal_MCValAlgALPs_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <MCTruthClassifier/MCTruthClassifier.h>

#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <Math/VectorUtil.h>

#include "MCVal/MCValUtils.h"

typedef ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>> LorentzVector;

class MCValAlgALPs : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MCValAlgALPs(const std::string &name, ISvcLocator *pSvcLocator);

  ~MCValAlgALPs() override;

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

  // Algorithm properties
  int m_pdgIdALP;
  std::string m_format;
  bool m_detector;

  // Event properties to save
  unsigned int m_runNumber = 0;         ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number

  float m_met;

  std::vector<float> *m_alp_pt;
  std::vector<float> *m_alp_eta;
  std::vector<float> *m_alp_phi;
  std::vector<float> *m_alp_m;
  std::vector<int> *m_alp_pdgid;

  std::vector<float> *m_mu_pt;
  std::vector<float> *m_mu_eta;
  std::vector<float> *m_mu_phi;
  std::vector<float> *m_mu_m;

private:
  unsigned int noALP = 0;
  // Configuration, and any other types of variables go here.
  // float m_cutValue;
  // TTree *m_myTree;
  // TH1 *m_myHist;

  /// \brief the accessor for \ref m_classificationDecoration
  std::unique_ptr<const SG::AuxElement::ConstAccessor<unsigned int> > m_mctcAccessor{};
};

float CosThetaStar(const xAOD::TruthParticle_v1::FourMom_t &muon1, const xAOD::TruthParticle_v1::FourMom_t &muon2)
{
    xAOD::TruthParticle_v1::FourMom_t MuonSystem = muon1 + muon2;
    float f = std::abs(MuonSystem.Pz()) / MuonSystem.Pz();
    float pp1 = muon1.E() + muon1.Pz();
    float pm1 = muon1.E() - muon1.Pz();
    float pp2 = muon2.E() + muon2.Pz();
    float pm2 = muon2.E() - muon2.Pz();

    return f / (MuonSystem.M() * std::sqrt(std::pow(MuonSystem.M(), 2) + std::pow(MuonSystem.Pt(), 2))) * (pp1 * pm2 - pp2 * pm1);
}

#endif
