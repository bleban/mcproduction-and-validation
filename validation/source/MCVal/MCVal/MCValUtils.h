#ifndef MCVal_TruthUtils
#define MCVal_TruthUtils

#include <TH2.h>
#include <TruthUtils/HepMCHelpers.h>
#include <TruthUtils/TruthClassifiers.h>

#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "AthContainers/ConstDataVector.h"

typedef ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>> LorentzVector;

//! \brief Truth namespace
namespace Truth
{

  const float GeV = 1000.;

  typedef ConstDataVector<xAOD::TruthParticleContainer> TruthPtcls;
  typedef ConstDataVector<xAOD::JetContainer> TruthJets;

  //! \name Helper methods to deal with truth particles.
  //@{

  //! /brief returns true if this is a stable "generator "particle according to the ATLAS defintion.
  //!        status shoudl be =1 (stable) and barcode < 200k (non-GEANT partcle)
  bool isStable(const xAOD::TruthParticle *ptcl);

  //! /return tlv from truth particle
  LorentzVector getLV(const xAOD::TruthParticle *ptcl);

  //! /brief returns false if the particle originates from a hadron
  bool notFromHadron(const xAOD::TruthParticle *ptcl);

  //! /brief returns true if the particle is a final electron
  bool isFinalElectron(const xAOD::TruthParticle *part);

  //! /brief returns true if the particle is a final muon
  bool isFinalMuon(const xAOD::TruthParticle *part);

  //! /brief returns true if the particle is a final W boson
  bool isFinalTau(const xAOD::TruthParticle *part);

  //! /brief returns true if the particle originates from a tau (recursively)
  bool isFromTau(const xAOD::TruthParticle *ptcl);

  //! /brief returns true if the particle originates from a gluon (recursively)
  bool isFromGluon(const xAOD::TruthParticle *ptcl);

  //! /brief returns true if the particle originates from a photon (recursively)
  bool isFromPhoton(const xAOD::TruthParticle *ptcl);

  //! /brief returns true if the particle originates from a particle with a given pdgid (recursively)
  bool isFromParticle(const xAOD::TruthParticle *ptcl, int pdgId);

  //! /brief returns true if the particle originates directly from a particle with a given pdgid
  bool isDirectlyFromParticle(const xAOD::TruthParticle *ptcl, int pdgId);

  //! /brief returns true if the particle is a final quark before hadronization
  bool isFinalQuark(const xAOD::TruthParticle *part);

  //! /brief sort particle pair by their pt
  inline static bool compareByPt(const xAOD::TruthParticle *a,
                                 const xAOD::TruthParticle *b)
  {
    return a->pt() > b->pt();
  }

  //! /brief get mctc classification
  void fillMCTC(TH2 *hist,
                const xAOD::TruthParticle *ptcl,
                unsigned int mctcResult);

  //! /brief returns list of truth HN particles originating from a dd
  TruthPtcls getHNsFromDDs(const xAOD::TruthParticle *ptcl, int &nGen);

  //! /brief checks if a particle falls inside the detector acceptance
  bool passDetector(const xAOD::TruthParticle *ptcl, float min_pt, float max_eta, bool crack_veto);

} // namespace Truth
#endif // MCVal_TruthUtils
