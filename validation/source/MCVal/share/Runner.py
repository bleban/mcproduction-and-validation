#!/usr/bin/env python

import os
import glob
import argparse
import subprocess
from sys import exit
import concurrent.futures
from typing import List, Tuple, Optional
from utils.logging import setup_logging

# Set up logging
logger = setup_logging("Runner.py", "info")
logger.info("Initialising script for concurrent job running...")


def run_job(command: str) -> bool:
    """Execute a shell command and stream output to console."""
    logger.info(f"Submitting job: {command}")
    try:
        subprocess.run(command, shell=True, check=True)
        logger.info(f"Job succeeded: {command}")
        return True
    except subprocess.CalledProcessError as e:
        logger.error(f"Job failed: {command} with return code {e.returncode}")
        return False


def parse_dsid_range(dsid_range: Optional[str]) -> Tuple[Optional[int], Optional[int]]:
    """Parse DSID range argument into a tuple of integers."""
    if dsid_range:
        try:
            min_dsid, max_dsid = map(int, dsid_range.split('-'))
            return min_dsid, max_dsid
        except ValueError:
            raise argparse.ArgumentTypeError("DSID range must be in the format 'minDSID-maxDSID'.")
    return None, None


def get_input_files(daod_path: str, format: str, min_dsid: Optional[int], max_dsid: Optional[int]) -> List[str]:
    """Retrieve input files from the specified DAOD path, applying format and DSID filtering."""
    input_file_paths = []

    if daod_path.startswith('/eos/'):
        try:
            from XRootD import client
        except ImportError:
            logger.error("XRootD not found. /eos/ directory can not be accessed. Exiting...")
            exit(1)

        server = 'root://eosuser.cern.ch'
        xrd_client = client.FileSystem(server)
        status, parents = xrd_client.dirlist(daod_path)

        for parent in parents:
            status, files = xrd_client.dirlist(os.path.join(daod_path, parent.name))

            if not status.ok:
                raise ValueError("Failed to list /eos/ directory. Ensure path is initialized correctly.")

            for file in files.dirlist:
                if file.name.startswith(f'DAOD_{format}'):
                    full_path = os.path.join(daod_path, parent.name, file.name)
                    if (min_dsid and max_dsid):
                        try:
                            dsid = int(file.name.split('.')[1])
                            if min_dsid <= dsid <= max_dsid:
                                input_file_paths.append(full_path)
                        except ValueError:
                            logger.warning(f"Skipping file {file.name}: Unable to extract DSID.")
                    else:
                        input_file_paths.append(full_path)
    else:
        input_file_paths = glob.glob(os.path.join(daod_path, '*', f'*{format}*.root'))
        if min_dsid and max_dsid:
            input_file_paths = [
                f for f in input_file_paths
                if min_dsid <= int(os.path.basename(f).split('.')[1]) <= max_dsid
            ]

    return input_file_paths


def main():
    """Main function to handle argument parsing and job execution."""
    parser = argparse.ArgumentParser(description="Run MCVal_eljob on DAOD files.")
    parser.add_argument('-i', '--daod-file-path', dest='daod_path', type=str, required=True, help='Input DAOD files path.')
    parser.add_argument('-f', '--format', dest='format', choices=['', 'TRUTH1', 'TRUTH3', 'PHYS'], default='', help='Format to run on.')
    parser.add_argument('-d', '--dsid', dest='dsid_range', type=str, help='DSID range to run on. Must be in the format "minDSID-maxDSID".')
    parser.add_argument('-l', '--log-level', dest='log_level', choices=['ALWAYS', 'FATAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG', 'VERBOSE'],
                        default='INFO', help='Logging level.')
    parser.add_argument('--detector', dest='detector', action=argparse.BooleanOptionalAction, default=False, help='Enable detector acceptance.')

    args = parser.parse_args()

    # Parse DSID range
    min_dsid, max_dsid = parse_dsid_range(args.dsid_range)

    # Get DAOD path (fallback to environment variable if not provided)
    daod_path = args.daod_path or os.environ.get('DAOD_PATH')
    if not daod_path:
        raise ValueError("DAOD file path must be specified via argument or DAOD_PATH environment variable.")

    # Handle ALPS condition
    if os.environ.get('TYPE') == 'ALPS' and args.format in ['', 'TRUTH3']:
        logger.warning("ALPS type only supports TRUTH1 format. Forcing TRUTH1.")
        args.format = 'TRUTH1'

    # Retrieve input files
    input_files = get_input_files(daod_path, args.format, min_dsid, max_dsid)
    if not input_files:
        logger.error("No matching input files found.")
        return

    # Create jobs
    jobs = [f"MCVal_eljob.py -i {file} -l {args.log_level}" + (" --detector" if args.detector else "") for file in input_files]

    # Execute jobs in parallel
    with concurrent.futures.ProcessPoolExecutor() as executor:
        results = list(executor.map(run_job, jobs))

    # Print summary
    successful_jobs = sum(results)
    failed_jobs = len(results) - successful_jobs
    logger.info(f"Completed {len(results)} jobs: {successful_jobs} succeeded, {failed_jobs} failed.")


if __name__ == '__main__':
    main()
