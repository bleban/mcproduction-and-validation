#!/usr/bin/env python

import os
import ROOT
import argparse
from typing import Tuple
from AnaAlgorithm.DualUseConfig import createAlgorithm


def setup_arg_parser() -> argparse.ArgumentParser:
    """Set up and return the argument parser."""
    parser = argparse.ArgumentParser(description="Run analysis on input files.")
    parser.add_argument('-i', '--input-file-path', dest='input_file_path',
                        required=True, type=str, help='Input file full path.')
    parser.add_argument('-l', '--log-level', dest='log_level', action='store',
                        default='INFO', type=str, help='Logging level.',
                        choices=['ALWAYS', 'FATAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG', 'VERBOSE'])
    parser.add_argument('--detector', dest='detector', action=argparse.BooleanOptionalAction,
                        default=False, help='Whether to consider detector acceptance.')
    return parser


def get_output_level(log_level: str) -> int:
    """Return the output level corresponding to the given log level."""
    output_levels = {
        'ALWAYS': 7,
        'FATAL': 6,
        'ERROR': 5,
        'WARNING': 4,
        'INFO': 3,
        'DEBUG': 2,
        'VERBOSE': 1
    }
    return output_levels.get(log_level, 3)


def setup_sample_handler(input_file_path: str) -> Tuple[ROOT.SH.SampleHandler, str, str]:
    """Set up and return the SampleHandler along with format and DSID."""
    sh = ROOT.SH.SampleHandler()
    sh.setMetaString('nc_tree', 'CollectionTree')
    input_file_name = os.path.basename(input_file_path)
    input_dir_path = os.path.dirname(input_file_path)
    dsid = input_file_name.split('.')[1]
    format = input_file_name.split('.')[0].split('_')[1]

    if input_dir_path.startswith('/eos/'):
        sh_list = ROOT.SH.DiskListXRD('eosuser.cern.ch', os.path.join('/', input_dir_path), True)
        ROOT.SH.ScanDir().filePattern(input_file_name).scan(sh, sh_list)
    else:
        ROOT.SH.ScanDir().filePattern(input_file_name).scan(sh, input_dir_path)

    sh.printContent()
    return sh, format, dsid


def create_algorithm(format: str, dsid: str, detector: bool, log_level: str) -> "AnaAlgorithm.PythonConfig.PythonConfig":
    """Create and return the algorithm based on the type."""
    type_env = os.environ.get('TYPE')
    if not type_env:
        raise ValueError('Environmental variable TYPE not set. Export to CASCADES, 2HNS, 4HNS, or ALPS!')

    alg_name = f'{format}_{dsid}'
    if type_env == 'CASCADES':
        alg = createAlgorithm('MCValAlgCASCADEs', alg_name)
        alg.pdgIdD0 = 9000005
        alg.pdgIdDP = 38
        alg.pdgIdDPP = 61
        alg.pdgIdChi = 62
    elif type_env in ('2HNS', '4HNS'):
        alg = createAlgorithm('MCValAlgHNs', alg_name)
        alg.pdgIdDD = 45
        alg.pdgIdN1 = 60
        alg.pdgIdN2 = 62
        alg.pdgIdN3 = 64
        alg.mediators = [alg.pdgIdDD, 23, 25, 32, 35, 55]
    elif type_env == 'ALPS':
        alg = createAlgorithm('MCValAlgALPs', alg_name)
        alg.pdgIdALP = 9000005
    else:
        raise ValueError(f'Unsupported TYPE: {type_env}. Export to CASCADES, 2HNS, 4HNS, or ALPS!')

    alg.format = format
    alg.detector = detector
    alg.OutputLevel = get_output_level(log_level)
    return alg


def main() -> None:
    """Main function to run the analysis job."""
    parser = setup_arg_parser()
    options = parser.parse_args()

    # Initialize ROOT xAOD.
    ROOT.xAOD.Init().ignore()

    # Set up the sample handler.
    sh, format, dsid = setup_sample_handler(options.input_file_path)

    # Create an EventLoop job.
    job = ROOT.EL.Job()
    job.sampleHandler(sh)
    # job.options().setDouble(ROOT.EL.Job.optMaxEvents, 100)
    job.outputAdd(ROOT.EL.OutputStream('ANALYSIS'))

    # Create and configure the algorithm.
    alg = create_algorithm(format, dsid, options.detector, options.log_level)

    # Add the algorithm to the job.
    job.algsAdd(alg)

    # Run the job using the direct driver.
    driver = ROOT.EL.DirectDriver()
    driver.submit(job, '_'.join(os.path.basename(options.input_file_path).split('.')[:3]))


if __name__ == "__main__":
    main()
