#include "MCVal/MCValUtils.h"

namespace Truth
{

  bool isStable(const xAOD::TruthParticle *ptcl)
  {
    return ptcl->status() == 1 && ptcl->barcode() < 200000;
  }

  // Return true if not from hadron
  bool notFromHadron(const xAOD::TruthParticle *ptcl)
  {
    int ID = ptcl->pdgId();

    // if the particle is a hadron, return false
    if (MC::isHadron(ID))
      return false;

    // if there are no parents, not from hadron
    if (ptcl->nParents() == 0)
      return true;

    const xAOD::TruthParticle *parent = ptcl->parent(0);
    int parentID = parent->pdgId();
    if (MC::isHadron(parentID))
      return false; // from hadron!
    if (parentID == 15 || parentID == ID)
      return notFromHadron(parent);

    return true;
  }

  LorentzVector getLV(const xAOD::TruthParticle *ptcl)
  {
    LorentzVector tempTLV(ptcl->pt(), ptcl->eta(), ptcl->phi(), ptcl->m());
    return tempTLV;
  }

  bool isFinalElectron(const xAOD::TruthParticle *part)
  {
    if (!MC::isElectron(part->pdgId()))
      return false;
    if (part->child(0) == nullptr)
      return true;
    for (unsigned int iChild = 0; iChild < part->nChildren(); iChild++)
    {
      if (MC::isElectron(part->child(iChild)->pdgId()))
        return false;
    }
    return true;
  }

  bool isFinalMuon(const xAOD::TruthParticle *part)
  {
    if (!MC::isMuon(part->pdgId()))
      return false;
    if (part->child(0) == nullptr)
      return true;
    for (unsigned int iChild = 0; iChild < part->nChildren(); iChild++)
    {
      if (MC::isMuon(part->child(iChild)->pdgId()))
        return false;
    }
    return true;
  }

  bool isFinalTau(const xAOD::TruthParticle *part)
  {
    if (!MC::isTau(part->pdgId()))
      return false;
    if (part->child(0) == nullptr)
      return false;
    for (unsigned int iChild = 0; iChild < part->nChildren(); iChild++)
    {
      if (MC::isTau(part->child(iChild)->pdgId()))
        return false;
    }
    return true;
  }

  bool isFromTau(const xAOD::TruthParticle *ptcl)
  {
    if (MC::isTau(ptcl->pdgId()))
      return true;
    if (ptcl->parent(0) == nullptr)
      return false;
    return isFromTau(ptcl->parent(0));
  }

  bool isFromGluon(const xAOD::TruthParticle *ptcl)
  {
    if (MC::isGluon(ptcl->pdgId()))
      return true;
    if (ptcl->parent(0) == nullptr)
      return false;
    if (MC::isGluon(ptcl->parent(0)->pdgId()))
      return true;
    if (ptcl->parent(0)->pdgId() != ptcl->pdgId())
      return false;
    return isFromGluon(ptcl->parent(0));
  }

  bool isFromPhoton(const xAOD::TruthParticle *ptcl)
  {
    if (MC::isPhoton(ptcl->pdgId()))
      return true;
    if (ptcl->parent(0) == nullptr)
      return false;
    if (MC::isPhoton(ptcl->parent(0)->pdgId()))
      return true;
    if (ptcl->parent(0)->pdgId() != ptcl->pdgId())
      return false;
    return isFromPhoton(ptcl->parent(0));
  }

  bool isFromParticle(const xAOD::TruthParticle *ptcl, int pdgId)
  {
    if (std::abs(ptcl->pdgId()) == pdgId)
      return true;
    if (ptcl->parent(0) == nullptr)
      return false;
    return isFromParticle(ptcl->parent(0), pdgId);
  }

  bool isDirectlyFromParticle(const xAOD::TruthParticle *ptcl, int pdgId)
  {
    if (std::abs(ptcl->parent(0)->pdgId()) == pdgId)
      return true;
    return false;
  }

  bool isFinalQuark(const xAOD::TruthParticle *part)
  {
    if (!MC::isQuark(part->pdgId()))
      return false;
    for (unsigned int iChild = 0; iChild < part->nChildren(); iChild++)
    {
      if (part->child(iChild) == nullptr)
        continue;
      if (part->child(iChild)->pdgId() == part->pdgId())
        return false;
    }
    return true;
  }

  void fillMCTC(TH2 *hist,
                const xAOD::TruthParticle *ptcl,
                unsigned int mctcResult)
  {
    std::string mctcVector = std::bitset<8>(ptcl->auxdata<unsigned int>("Classification")).to_string();
    for (unsigned int i = 0; i < 9; i++)
    {
      if (i == 0)
      {
        hist->Fill(i, MCTruthPartClassifier::isPrompt(mctcResult, true) ? 1 : 0);
      }
      else
      {
        hist->Fill(i, mctcVector[i] - '0');
      }
    }
  }

  TruthPtcls getHNsFromDDs(const xAOD::TruthParticle *ptcl, int &nGen)
  {
    nGen++;
    TruthPtcls descendants(SG::VIEW_ELEMENTS);

    if (ptcl->pdgId() != ptcl->child(0)->pdgId())
    {
      for (size_t ichild = 0; ichild < ptcl->nChildren(); ++ichild)
      {
        descendants.push_back(ptcl->child(ichild));
      }
    }
    else
    {
      TruthPtcls childDescendants = getHNsFromDDs(ptcl->child(0), nGen);
      descendants.insert(descendants.end(), childDescendants.begin(), childDescendants.end());
    }
    return descendants;
  }

  //! /brief checks if a particle falls inside the detector acceptance
  bool passDetector(const xAOD::TruthParticle *ptcl, float min_pt, float max_eta, bool crack_veto)
  {
    if (ptcl->pt() / Truth::GeV < min_pt)
      return false;
    if (std::abs(ptcl->eta()) > max_eta)
      return false;
    if (crack_veto)
    {
      if ((std::abs(ptcl->eta()) > 1.37) && (std::abs(ptcl->eta()) < 1.52))
        return false;
    }
    return true;
  }
} // namespace Truth
