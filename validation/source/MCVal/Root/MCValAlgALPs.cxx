#include <AsgMessaging/MessageCheck.h>
#include <MCVal/MCValAlgALPs.h>

using ROOT::Math::VectorUtil::DeltaPhi;
using ROOT::Math::VectorUtil::DeltaR;

MCValAlgALPs::MCValAlgALPs(const std::string &name,
                           ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm(name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. This is also where you
  // declare all properties for your algorithm. Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("pdgIdALP", m_pdgIdALP = 9000005, "Truth ALP PDGId");
  declareProperty("format", m_format = "TRUTH1", "Derivation format");
  declareProperty("detector", m_detector = false, "Switch on the detector acceptance");
}

StatusCode MCValAlgALPs::initialize()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees. This method gets called before any input files are
  // connected.

  // Bins for displacement
  double start = 1e-15;
  double end = 1e-5;
  const int N = 20;
  double xbins[N + 1];
  double f = std::pow(end / start, 1.0 / N);
  for (int i = 0; i <= N; i++)
  {
    xbins[i] = start * std::pow(f, i);
  }

  // Create output tree
  ANA_CHECK(book(TTree("analysis", "My analysis ntuple")));
  TTree *mytree = tree("analysis");

  // Declare branches
  mytree->Branch("RunNumber", &m_runNumber);
  mytree->Branch("EventNumber", &m_eventNumber);

  // Prepare branches for ALP
  m_alp_pt = new std::vector<float>();
  m_alp_eta = new std::vector<float>();
  m_alp_phi = new std::vector<float>();
  m_alp_m = new std::vector<float>();
  m_alp_pdgid = new std::vector<int>();

  mytree->Branch("alp_pt", &m_alp_pt);
  mytree->Branch("alp_eta", &m_alp_eta);
  mytree->Branch("alp_phi", &m_alp_phi);
  mytree->Branch("alp_m", &m_alp_m);
  mytree->Branch("alp_pdgid", &m_alp_pdgid);

  // Prepare branches for muons
  m_mu_pt = new std::vector<float>();
  m_mu_eta = new std::vector<float>();
  m_mu_phi = new std::vector<float>();
  m_mu_m = new std::vector<float>();

  mytree->Branch("mu_pt", &m_mu_pt);
  mytree->Branch("mu_eta", &m_mu_eta);
  mytree->Branch("mu_phi", &m_mu_phi);
  mytree->Branch("mu_m", &m_mu_m);

  // Book output histograms
  ANA_CHECK(book(TH1F("h_N_alp", "h_N_alp", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_pdgid_alp", "h_pdgid_alp", 3, 3.5, 6.5)));
  ANA_CHECK(book(TH1F("h_m_alp", "h_m_alp", 210, -0.5, 209.5)));
  ANA_CHECK(book(TH2F("h_mctc_alp", "h_mctc_alp", 8, -0.5, 7.5, 2, -0.5, 1.5)));

  ANA_CHECK(book(TH1F("h_N_mu_alps", "h_N_mu_alps", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_pt_mu", "h_pt_mu", 110, -0.5, 109.5)));
  ANA_CHECK(book(TH1F("h_eta_mu", "h_eta_mu", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_mu", "h_phi_mu", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH2F("h_mctc_mu", "h_mctc_mu", 8, -0.5, 7.5, 2, -0.5, 1.5)));

  ANA_CHECK(book(TH1F("h_displacement", "h_displacement", N, xbins)));
  ANA_CHECK(book(TH1F("h_displacement_trans", "h_displacement_trans", N, xbins)));
  ANA_CHECK(book(TH1F("h_displacement_long", "h_displacement_long", 100, -1., 1.)));

  ANA_CHECK(book(TH1F("h_m_mumu", "h_m_mumu", 210, -0.5, 209.)));
  ANA_CHECK(book(TH1F("h_dR_mumu", "h_dR_mumu", 100, 0, 6.28)));
  ANA_CHECK(book(TH1F("h_aco_mumu", "h_aco_mumu", 100, 0, 0.1)));
  ANA_CHECK(book(TH1F("h_costh_mumu", "h_costh_mumu", 100, -1.0, 1.0)));

  m_mctcAccessor = std::make_unique<SG::AuxElement::ConstAccessor<unsigned int> > ("MCTCClassification");

  return StatusCode::SUCCESS;
}

StatusCode MCValAlgALPs::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees. This is where most of your actual analysis
  // code will go.

  // Truth particles
  std::string branch = m_format == "TRUTH1" ? "TruthParticles" : "TruthBSMWithDecayParticles";
  const xAOD::EventInfo *eventInfo = nullptr;
  const xAOD::TruthParticleContainer *truth_particles = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
  ANA_CHECK(evtStore()->retrieve(truth_particles, branch));

  std::vector<float> vec_displacement;
  std::vector<const xAOD::TruthParticle *> vec_alp;
  std::vector<const xAOD::TruthParticle *> vec_mu;
  std::vector<const xAOD::TruthParticle *> vec_q;

  for (const auto tp : *truth_particles)
  {

    // Skip particles that come from hadron decays
    if (!Truth::notFromHadron(tp))
      continue;

    // Skip gluons
    if (MC::isGluon(tp->pdgId()))
      continue;

    // BSM particles
    if (std::abs(tp->pdgId()) == m_pdgIdALP)
    {
      // Skip if it is a self-decay
      if (tp->pdgId() == tp->child(0)->pdgId())
        continue;

      // Store ALP particle
      vec_alp.push_back(tp);

      // Calculate the displacement
      if (tp->hasProdVtx() && tp->hasDecayVtx())
      {
        TVector3 prod_pos(tp->prodVtx()->x(), tp->prodVtx()->y(), tp->prodVtx()->z());
        TVector3 decay_pos(tp->decayVtx()->x(), tp->decayVtx()->y(), tp->decayVtx()->z());
        double d = (decay_pos - prod_pos).Mag();
        vec_displacement.push_back(d);
        ANA_MSG_DEBUG("Displacement of the " << tp->pdgId() << " particle: " << d << " mm.");
      }

      // Store ALP decay products (mu mu)
      ANA_MSG_DEBUG("Number of " << tp->pdgId() << " children: " << tp->nChildren() << ":");
      for (unsigned int iChild = 0; iChild < tp->nChildren(); iChild++)
      {
        ANA_MSG_DEBUG("\t- " << tp->child(iChild)->pdgId());
      }
    }

    // All final state muons
    if (MC::isMuon(tp->pdgId()) && (tp->pdgId() != tp->parent(0)->pdgId()))
    {
      // Skip if the muon falls out of the detector acceptance if requested by m_detector
      if (m_detector && !Truth::passDetector(tp, 3., 2.7, true))
        continue;

      vec_mu.push_back(tp);
    }
  }

  // Count events that do not contain mediating ALP
  if (vec_alp.size() < 1)
  {
    ANA_MSG_DEBUG("Event " << eventInfo->eventNumber() << " without ALP.");
    noALP++;
  }

  unsigned int mctcResult{};
  hist("h_N_alp")->Fill(vec_alp.size());
  for (auto alp : vec_alp)
  {
    hist("h_m_alp")->Fill(alp->m() / Truth::GeV);
    hist("h_pdgid_alp")->Fill(alp->pdgId() - 9000000);
    if (m_mctcAccessor->isAvailable(*alp))
    {
      mctcResult = (*m_mctcAccessor)(*alp);
    }
    Truth::fillMCTC(hist2d("h_mctc_alp"), alp, mctcResult);
  }

  for (auto displacement : vec_displacement)
  {
    hist("h_displacement")->Fill(displacement);
  }

  hist("h_N_mu_alps")->Fill(vec_mu.size());
  LorentzVector sum_mm;
  for (auto mu : vec_mu)
  {
    sum_mm += Truth::getLV(mu);

    hist("h_pt_mu")->Fill(mu->pt() / Truth::GeV);
    hist("h_eta_mu")->Fill(mu->eta());
    hist("h_phi_mu")->Fill(mu->phi());
    hist("h_displacement_trans")->Fill(mu->prodVtx()->perp());
    hist("h_displacement_long")->Fill(mu->prodVtx()->z() * std::sin(mu->p4().Theta()));
    if (m_mctcAccessor->isAvailable(*mu))
    {
      mctcResult = (*m_mctcAccessor)(*mu);
    }
    Truth::fillMCTC(hist2d("h_mctc_mu"), mu, mctcResult);
  }
  hist("h_m_mumu")->Fill(sum_mm.M() / Truth::GeV);
  hist("h_dR_mumu")->Fill(DeltaR(vec_mu.at(0)->p4(), vec_mu.at(1)->p4()));
  hist("h_aco_mumu")->Fill(ROOT::Math::Pi() - DeltaPhi(vec_mu.at(0)->p4(), vec_mu.at(1)->p4()));
  hist("h_costh_mumu")->Fill(CosThetaStar(vec_mu.at(0)->p4(), vec_mu.at(1)->p4()));

  m_alp_pt->clear();
  m_alp_eta->clear();
  m_alp_phi->clear();
  m_alp_m->clear();
  m_alp_pdgid->clear();

  for (auto alp : vec_alp)
  {
    m_alp_pt->push_back(alp->pt());
    m_alp_eta->push_back(alp->eta());
    m_alp_phi->push_back(alp->phi());
    m_alp_m->push_back(alp->m());
    m_alp_pdgid->push_back(alp->pdgId());
  }

  m_mu_pt->clear();
  m_mu_eta->clear();
  m_mu_phi->clear();
  m_mu_m->clear();

  for (auto mu : vec_mu)
  {
    m_mu_pt->push_back(mu->pt());
    m_mu_eta->push_back(mu->eta());
    m_mu_phi->push_back(mu->phi());
    m_mu_m->push_back(mu->m());
  }

  tree("analysis")->Fill();

  if (m_eventNumber > 0 && m_eventNumber % 1000 == 0)
  {
    ANA_MSG_INFO("Done " << m_eventNumber << " events.");
  }
  m_eventNumber++;
  return StatusCode::SUCCESS;
}

StatusCode MCValAlgALPs::finalize()
{
  ANA_MSG_INFO("================== SUMMARY =================");
  ANA_MSG_INFO("Number of events without the mediating axion-like-particle: " << noALP);
  ANA_MSG_INFO("============== END OF SUMMARY ==============");
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk. This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

MCValAlgALPs::~MCValAlgALPs()
{

  // Delete the allocated vectors to avoid memory leaks
  delete m_alp_pt;
  delete m_alp_eta;
  delete m_alp_phi;
  delete m_alp_m;
  delete m_alp_pdgid;

  delete m_mu_pt;
  delete m_mu_eta;
  delete m_mu_phi;
  delete m_mu_m;
}
