#include <AsgMessaging/MessageCheck.h>
#include <MCVal/MCValAlgHNs.h>

using ROOT::Math::VectorUtil::DeltaR;

MCValAlgHNs::MCValAlgHNs(const std::string &name,
                         ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm(name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. This is also where you
  // declare all properties for your algorithm. Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("pdgIdDD", m_pdgIdDD = 45, "Truth dd PDGId");
  declareProperty("pdgIdN1", m_pdgIdN1 = 60, "Truth N1 PDGId");
  declareProperty("pdgIdN2", m_pdgIdN2 = 62, "Truth N2 PDGId");
  declareProperty("pdgIdN3", m_pdgIdN3 = 64, "Truth N3 PDGId");
  declareProperty("mediators", m_mediators = {m_pdgIdDD, 23, 25, 32, 35, 55}, "PDGIds of other mediators");
  declareProperty("format", m_format = "TRUTH1", "Derivation format");
  declareProperty("detector", m_detector = false, "Switch on the detector acceptance");
}

StatusCode MCValAlgHNs::initialize()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees. This method gets called before any input files are
  // connected.

  // Bins for displacement
  double start = 1e-5;
  double end = 1e2;
  const int N = 20;
  double xbins[N + 1];
  double f = std::pow(end / start, 1.0 / N);
  for (int i = 0; i <= N; i++)
  {
    xbins[i] = start * std::pow(f, i);
  }

  // Create output tree
  ANA_CHECK(book(TTree("analysis", "My analysis ntuple")));
  TTree *mytree = tree("analysis");

  // Declare branches
  mytree->Branch("RunNumber", &m_runNumber);
  mytree->Branch("EventNumber", &m_eventNumber);

  // Prepare branches for dd
  m_dd_pt = new std::vector<float>();
  m_dd_eta = new std::vector<float>();
  m_dd_phi = new std::vector<float>();
  m_dd_m = new std::vector<float>();
  m_dd_pdgid = new std::vector<int>();

  mytree->Branch("dd_pt", &m_dd_pt);
  mytree->Branch("dd_eta", &m_dd_eta);
  mytree->Branch("dd_phi", &m_dd_phi);
  mytree->Branch("dd_m", &m_dd_m);
  mytree->Branch("dd_pdgid", &m_dd_pdgid);

  // Prepare branches for N1
  m_n1_pt = new std::vector<float>();
  m_n1_eta = new std::vector<float>();
  m_n1_phi = new std::vector<float>();
  m_n1_m = new std::vector<float>();
  m_n1_pdgid = new std::vector<int>();

  mytree->Branch("N1_pt", &m_n1_pt);
  mytree->Branch("N1_eta", &m_n1_eta);
  mytree->Branch("N1_phi", &m_n1_phi);
  mytree->Branch("N1_m", &m_n1_m);
  mytree->Branch("N1_pdgid", &m_n1_pdgid);

  // Prepare branches for N2
  m_n2_pt = new std::vector<float>();
  m_n2_eta = new std::vector<float>();
  m_n2_phi = new std::vector<float>();
  m_n2_m = new std::vector<float>();
  m_n2_pdgid = new std::vector<int>();

  mytree->Branch("N2_pt", &m_n2_pt);
  mytree->Branch("N2_eta", &m_n2_eta);
  mytree->Branch("N2_phi", &m_n2_phi);
  mytree->Branch("N2_m", &m_n2_m);
  mytree->Branch("N2_pdgid", &m_n2_pdgid);

  // Prepare branches for N3
  m_n3_pt = new std::vector<float>();
  m_n3_eta = new std::vector<float>();
  m_n3_phi = new std::vector<float>();
  m_n3_m = new std::vector<float>();
  m_n3_pdgid = new std::vector<int>();

  mytree->Branch("N3_pt", &m_n3_pt);
  mytree->Branch("N3_eta", &m_n3_eta);
  mytree->Branch("N3_phi", &m_n3_phi);
  mytree->Branch("N3_m", &m_n3_m);
  mytree->Branch("N3_pdgid", &m_n3_pdgid);

  // Prepare branches for leptons
  m_lep_pt = new std::vector<float>();
  m_lep_eta = new std::vector<float>();
  m_lep_phi = new std::vector<float>();
  m_lep_m = new std::vector<float>();
  m_lep_pdgid = new std::vector<int>();

  mytree->Branch("lep_pt", &m_lep_pt);
  mytree->Branch("lep_eta", &m_lep_eta);
  mytree->Branch("lep_phi", &m_lep_phi);
  mytree->Branch("lep_m", &m_lep_m);
  mytree->Branch("lep_pdgid", &m_lep_pdgid);

  // Prepare branches for electrons
  m_el_pt = new std::vector<float>();
  m_el_eta = new std::vector<float>();
  m_el_phi = new std::vector<float>();
  m_el_m = new std::vector<float>();

  mytree->Branch("el_pt", &m_el_pt);
  mytree->Branch("el_eta", &m_el_eta);
  mytree->Branch("el_phi", &m_el_phi);
  mytree->Branch("el_m", &m_el_m);

  // Prepare branches for muons
  m_mu_pt = new std::vector<float>();
  m_mu_eta = new std::vector<float>();
  m_mu_phi = new std::vector<float>();
  m_mu_m = new std::vector<float>();

  mytree->Branch("mu_pt", &m_mu_pt);
  mytree->Branch("mu_eta", &m_mu_eta);
  mytree->Branch("mu_phi", &m_mu_phi);
  mytree->Branch("mu_m", &m_mu_m);

  // Prepare branches for taus
  m_tau_pt = new std::vector<float>();
  m_tau_eta = new std::vector<float>();
  m_tau_phi = new std::vector<float>();
  m_tau_m = new std::vector<float>();

  mytree->Branch("tau_pt", &m_tau_pt);
  mytree->Branch("tau_eta", &m_tau_eta);
  mytree->Branch("tau_phi", &m_tau_phi);
  mytree->Branch("tau_m", &m_tau_m);

  // Prepare branches for quraks
  m_q_pt = new std::vector<float>();
  m_q_eta = new std::vector<float>();
  m_q_phi = new std::vector<float>();
  m_q_m = new std::vector<float>();
  m_q_pdgid = new std::vector<int>();

  mytree->Branch("q_pt", &m_q_pt);
  mytree->Branch("q_eta", &m_q_eta);
  mytree->Branch("q_phi", &m_q_phi);
  mytree->Branch("q_m", &m_q_m);
  mytree->Branch("q_pdgid", &m_q_pdgid);

  // Prepare branches for jets
  m_jet_pt = new std::vector<float>();
  m_jet_eta = new std::vector<float>();
  m_jet_phi = new std::vector<float>();
  m_jet_m = new std::vector<float>();

  mytree->Branch("jet_pt", &m_jet_pt);
  mytree->Branch("jet_eta", &m_jet_eta);
  mytree->Branch("jet_phi", &m_jet_phi);
  mytree->Branch("jet_m", &m_jet_m);

  // Prepare branches for MET
  mytree->Branch("met", &m_met);

  // Book output histograms
  ANA_CHECK(book(TH1F("h_N_dd", "h_N_dd", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_N_mediators", "h_N_mediators", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_N_n1", "h_N_n1", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_N_n2", "h_N_n2", 5, -0.5, 4.5)));
  ANA_CHECK(book(TH1F("h_N_n3", "h_N_n3", 5, -0.5, 4.5)));

  ANA_CHECK(book(TH1F("h_pdgid_dd", "h_pdgid_dd", 3, 43.5, 46.5)));
  ANA_CHECK(book(TH1F("h_pdgid_mediators", "h_pdgid_mediators", 35, 22.5, 57.5)));
  ANA_CHECK(book(TH1F("h_pdgid_n1", "h_pdgid_n1", 3, 58.5, 61.5)));
  ANA_CHECK(book(TH1F("h_pdgid_n2", "h_pdgid_n2", 3, 60.5, 63.5)));
  ANA_CHECK(book(TH1F("h_pdgid_n3", "h_pdgid_n3", 3, 62.5, 65.5)));

  ANA_CHECK(book(TH1F("h_m_dd", "h_m_dd", 150, 49.5, 199.5)));
  ANA_CHECK(book(TH1F("h_m_n1", "h_m_n1", 100, -0.5, 99.5)));
  ANA_CHECK(book(TH1F("h_m_n2", "h_m_n2", 100, -0.5, 99.5)));
  ANA_CHECK(book(TH1F("h_m_n3", "h_m_n3", 100, -0.5, 99.5)));
  ANA_CHECK(book(TH1F("h_m_ddDecayProductsNN", "h_m_ddDecayProductsNN", 150, 49.5, 199.5)));
  ANA_CHECK(book(TH1F("h_m_ddDecayProducts2l4j", "h_m_ddDecayProducts2l4j", 150, 49.5, 199.5)));
  ANA_CHECK(book(TH1F("h_m_nDecayProducts", "h_m_nDecayProducts", 100, -0.5, 99.5)));
  ANA_CHECK(book(TH1F("h_dR_nn", "h_dR_nn", 32, 0, 6.4)));

  ANA_CHECK(book(TH1F("h_N_el_from_n", "h_N_el_from_n", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_N_el_from_n1", "h_N_el_from_n1", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_pt_el", "h_pt_el", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_el_lead", "h_pt_el_lead", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_el_sublead", "h_pt_el_sublead", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_el", "h_eta_el", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_eta_el_lead", "h_eta_el_sublead", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_eta_el_sublead", "h_eta_el_sublead", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_el", "h_phi_el", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_phi_el_lead", "h_phi_el_sublead", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_phi_el_sublead", "h_phi_el_sublead", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_dR_elel", "h_dR_elel", 32, 0, 6.4)));

  ANA_CHECK(book(TH1F("h_N_mu_from_n", "h_N_mu_from_n", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_N_mu_from_n2", "h_N_mu_from_n2", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_pt_mu", "h_pt_mu", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_mu_lead", "h_pt_mu_lead", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_mu_sublead", "h_pt_mu_sublead", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_mu", "h_eta_mu", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_eta_mu_lead", "h_eta_mu_lead", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_eta_mu_sublead", "h_eta_mu_sublead", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_mu", "h_phi_mu", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_phi_mu_lead", "h_phi_mu_lead", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_phi_mu_sublead", "h_phi_mu_sublead", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_dR_mumu", "h_dR_mumu", 32, 0, 6.4)));

  ANA_CHECK(book(TH1F("h_N_tau_from_n", "h_N_tau_from_n", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_N_tau_from_n3", "h_N_tau_from_n3", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_pt_tau", "h_pt_tau", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_tau_lead", "h_pt_tau_lead", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_tau_sublead", "h_pt_tau_sublead", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_tau", "h_eta_tau", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_eta_tau_lead", "h_eta_tau_lead", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_eta_tau_sublead", "h_eta_tau_sublead", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_tau", "h_phi_tau", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_phi_tau_lead", "h_phi_tau_lead", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_phi_tau_sublead", "h_phi_tau_sublead", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_dR_tautau", "h_dR_tautau", 32, 0, 6.4)));

  ANA_CHECK(book(TH1F("h_N_lep_from_n", "h_N_lep_from_n", 10, -0.5, 9.5)));
  ANA_CHECK(book(TH1F("h_pdgid_lep", "h_pdgid_lep", 33, -16.5, 16.5)));
  ANA_CHECK(book(TH1F("h_pt_lep", "h_pt_lep", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_lep_lead", "h_pt_lep_lead", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_lep_sublead", "h_pt_lep_sublead", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_lep", "h_eta_lep", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_eta_lep_lead", "h_eta_lep_lead", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_eta_lep_sublead", "h_eta_lep_sublead", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_lep", "h_phi_lep", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_phi_lep_lead", "h_phi_lep_lead", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_phi_lep_sublead", "h_phi_lep_sublead", 32, -3.2, 3.2)));
  ANA_CHECK(book(TH1F("h_dR_leplep", "h_dR_leplep", 32, 0, 6.4)));
  ANA_CHECK(book(TH1F("h_pTratio_lep", "h_pTratio_lep", 55, -0.05, 1.05)));

  ANA_CHECK(book(TH1F("h_displacement", "h_displacement", N, xbins)));
  ANA_CHECK(book(TH1F("h_displacement_trans", "h_displacement_trans", N, xbins)));
  ANA_CHECK(book(TH1F("h_displacement_long", "h_displacement_long", 100, -1., 1.)));

  ANA_CHECK(book(TH2F("h_N_el_vs_N_mu", "h_N_el_vs_N_mu", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_el_vs_N_tau", "h_N_el_vs_N_tau", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_mu_vs_N_tau", "h_N_mu_vs_N_tau", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_eln1_vs_N_mun1", "h_N_eln1_vs_N_mun1", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_eln1_vs_N_taun1", "h_N_eln1_vs_N_taun1", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_mun1_vs_N_taun1", "h_N_mun1_vs_N_taun1", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_eln2_vs_N_mun2", "h_N_eln2_vs_N_mun2", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_eln2_vs_N_taun2", "h_N_eln2_vs_N_taun2", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_mun2_vs_N_taun2", "h_N_mun2_vs_N_taun2", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_eln3_vs_N_mun3", "h_N_eln3_vs_N_mun3", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_eln3_vs_N_taun3", "h_N_eln3_vs_N_taun3", 5, -0.5, 4.5, 5, -0.5, 4.5)));
  ANA_CHECK(book(TH2F("h_N_mun3_vs_N_taun3", "h_N_mun3_vs_N_taun3", 5, -0.5, 4.5, 5, -0.5, 4.5)));

  ANA_CHECK(book(TH2F("h_mctc_dd", "h_mctc_dd", 9, -0.5, 8.5, 2, -0.5, 1.5)));
  ANA_CHECK(book(TH2F("h_mctc_n1", "h_mctc_n1", 9, -0.5, 8.5, 2, -0.5, 1.5)));
  ANA_CHECK(book(TH2F("h_mctc_n2", "h_mctc_n2", 9, -0.5, 8.5, 2, -0.5, 1.5)));
  ANA_CHECK(book(TH2F("h_mctc_n3", "h_mctc_n3", 9, -0.5, 8.5, 2, -0.5, 1.5)));
  ANA_CHECK(book(TH2F("h_mctc_el", "h_mctc_el", 9, -0.5, 8.5, 2, -0.5, 1.5)));
  ANA_CHECK(book(TH2F("h_mctc_mu", "h_mctc_mu", 9, -0.5, 8.5, 2, -0.5, 1.5)));
  ANA_CHECK(book(TH2F("h_mctc_tau", "h_mctc_tau", 9, -0.5, 8.5, 2, -0.5, 1.5)));

  ANA_CHECK(book(TH1F("h_N_q_hns", "h_N_q_hns", 11, -0.5, 10.5)));
  ANA_CHECK(book(TH1F("h_pdgid_q", "h_pdgid_q", 15, -7.5, 7.5)));
  ANA_CHECK(book(TH1F("h_pt_q", "h_pt_q", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_eta_q", "h_eta_q", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_q", "h_phi_q", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH1F("h_N_jet", "h_N_jet", 16, -0.5, 15.5)));
  ANA_CHECK(book(TH1F("h_pt_jet", "h_pt_jet", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pTratio_jet", "h_pTratio_jet", 62, -0.05, 3.05)));
  ANA_CHECK(book(TH1F("h_pt_jet_lead", "h_pt_jet_lead", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_jet_sublead", "h_pt_jet_sublead", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_jet_3", "h_pt_jet_3", 1000, 0, 1000)));
  ANA_CHECK(book(TH1F("h_pt_jet_4", "h_pt_jet_4", 1000, 0, 1000)));
  ANA_CHECK(book(TH2F("h_ptjet1_vs_ptjet2", "h_ptjet1_vs_ptjet2", 20, 20., 60., 20, 20., 60.)));
  ANA_CHECK(book(TH1F("h_eta_jet", "h_eta_jet", 40, -4.0, 4.0)));
  ANA_CHECK(book(TH1F("h_phi_jet", "h_phi_jet", 32, -3.2, 3.2)));

  ANA_CHECK(book(TH1F("h_met", "h_met", 1000, 0, 1000)));

  m_mctcAccessor = std::make_unique<SG::AuxElement::ConstAccessor<unsigned int> > ("MCTCClassification");

  return StatusCode::SUCCESS;
}

StatusCode MCValAlgHNs::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees. This is where most of your actual analysis
  // code will go.

  // Truth particles
  std::string branch = m_format == "TRUTH1" ? "TruthParticles" : "TruthBSMWithDecayParticles";
  const xAOD::EventInfo *eventInfo = nullptr;
  const xAOD::TruthParticleContainer *truth_particles = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
  ANA_CHECK(evtStore()->retrieve(truth_particles, branch));

  std::vector<float> NDecayProducts;
  std::vector<float> ddDecayProductsNN;
  std::vector<float> ddDecayProducts2l4j;
  std::vector<float> vec_jet_pt;
  std::vector<TVector3> vec_displacement;
  std::vector<const xAOD::TruthParticle *> vec_dd;
  std::vector<const xAOD::TruthParticle *> vec_mediators;
  std::vector<const xAOD::TruthParticle *> vec_n1;
  std::vector<const xAOD::TruthParticle *> vec_n2;
  std::vector<const xAOD::TruthParticle *> vec_n3;
  std::vector<const xAOD::TruthParticle *> vec_el;
  std::vector<const xAOD::TruthParticle *> vec_el_from_n1;
  std::vector<const xAOD::TruthParticle *> vec_el_from_n2;
  std::vector<const xAOD::TruthParticle *> vec_el_from_n3;
  std::vector<const xAOD::TruthParticle *> vec_mu;
  std::vector<const xAOD::TruthParticle *> vec_mu_from_n1;
  std::vector<const xAOD::TruthParticle *> vec_mu_from_n2;
  std::vector<const xAOD::TruthParticle *> vec_mu_from_n3;
  std::vector<const xAOD::TruthParticle *> vec_tau;
  std::vector<const xAOD::TruthParticle *> vec_tau_from_n1;
  std::vector<const xAOD::TruthParticle *> vec_tau_from_n2;
  std::vector<const xAOD::TruthParticle *> vec_tau_from_n3;
  std::vector<const xAOD::TruthParticle *> vec_lep;
  std::vector<const xAOD::TruthParticle *> vec_q;

  for (const auto tp : *truth_particles)
  {

    // Skip particles that come from hadron decays
    if (!Truth::notFromHadron(tp))
      continue;

    // Skip gluons
    if (MC::isGluon(tp->pdgId()))
      continue;

    // BSM particles
    if (std::find(m_mediators.begin(), m_mediators.end(), std::abs(tp->pdgId())) != m_mediators.end() && tp->nChildren() > 0)
    {
      // Skip if it is a self-decay
      if (tp->pdgId() == tp->child(0)->pdgId())
        continue;

      // Store mediator particles
      std::abs(tp->pdgId()) == m_pdgIdDD ? vec_dd.push_back(tp) : vec_mediators.push_back(tp);

      // Store dd decay products (NN and 2l4j)
      int nGen = 0;
      LorentzVector sumNN, sum2l4j;
      Truth::TruthPtcls HNs = Truth::getHNsFromDDs(tp, nGen);

      ANA_MSG_DEBUG("Number of " << tp->pdgId() << " children: " << HNs.size() << " after " << nGen << " generations. Event number: " << eventInfo->eventNumber());
      for (auto hns : HNs)
      {
        // Store N particles
        sumNN += Truth::getLV(hns);
        if (std::abs(hns->pdgId()) == m_pdgIdN1)
        {
          vec_n1.push_back(hns);
        }
        else if (std::abs(hns->pdgId()) == m_pdgIdN2)
        {
          vec_n2.push_back(hns);
        }
        else if (std::abs(hns->pdgId()) == m_pdgIdN3)
        {
          vec_n3.push_back(hns);
        }
        if (hns->hasProdVtx() && hns->hasDecayVtx())
        {
          TVector3 prod_pos(hns->prodVtx()->x(), hns->prodVtx()->y(), hns->prodVtx()->z());
          TVector3 decay_pos(hns->decayVtx()->x(), hns->decayVtx()->y(), hns->decayVtx()->z());
          TVector3 d = decay_pos - prod_pos;
          ANA_MSG_DEBUG("Decay length of the " << hns->pdgId() << " particle: " << d.Mag() << " mm. Decay vertex position:");
          ANA_MSG_DEBUG("\tx: " << d.X() << ", y: " << d.Y() << ", z: " << d.Z() << ", Perp: " << d.Perp() << ".");
          vec_displacement.push_back(d);
        }

        ANA_MSG_DEBUG("Particle " << hns->pdgId() << " has " << hns->nChildren() << " children:");

        // Loop through N decay products
        LorentzVector sumljj;
        for (unsigned int iChild = 0; iChild < hns->nChildren(); iChild++)
        {
          const xAOD::TruthParticle *n_child = hns->child(iChild);
          ANA_MSG_DEBUG("\t- " << n_child->pdgId());

          if (MC::isElectron(n_child->pdgId()) && !Truth::isFromPhoton(n_child) && !Truth::isFromTau(n_child))
          {
            // Skip if it is a self-decay or is not stable
            if ((n_child->parent(0)->pdgId() == n_child->pdgId()) || !Truth::isStable(n_child))
              continue;

            // Skip if the particle falls out of the detector acceptance if requested by m_detector
            if (m_detector && !Truth::passDetector(n_child, 4.5, 2.47, true))
              continue;

            if (Truth::isDirectlyFromParticle(n_child, m_pdgIdN2))
            {
              ANA_MSG_DEBUG("Electron from N2!!!");
              vec_el_from_n2.push_back(n_child);
              elFromN2++;
            }
            else if (Truth::isDirectlyFromParticle(n_child, m_pdgIdN3))
            {
              ANA_MSG_DEBUG("Electron from N3!!!");
              vec_el_from_n3.push_back(n_child);
              elFromN3++;
            }
            else if (Truth::isDirectlyFromParticle(n_child, m_pdgIdN1))
            {
              vec_el_from_n1.push_back(n_child);
            }

            vec_el.push_back(n_child);
            vec_lep.push_back(n_child);
          }

          if (MC::isMuon(n_child->pdgId()) && !Truth::isFromPhoton(n_child) && !Truth::isFromTau(n_child))
          {
            // Skip if it is a self-decay
            if ((n_child->parent(0)->pdgId() == n_child->pdgId()) || !Truth::isStable(n_child))
              continue;

            // Skip if the particle falls out of the detector acceptance if requested by m_detector
            if (m_detector && !Truth::passDetector(n_child, 3., 2.7, true))
              continue;

            if (Truth::isDirectlyFromParticle(n_child, m_pdgIdN1))
            {
              ANA_MSG_DEBUG("Muon from N1!!!");
              vec_mu_from_n1.push_back(n_child);
              muFromN1++;
            }
            else if (Truth::isDirectlyFromParticle(n_child, m_pdgIdN3))
            {
              ANA_MSG_DEBUG("Muon from N3!!!");
              vec_mu_from_n3.push_back(n_child);
              muFromN3++;
            }
            else if (Truth::isDirectlyFromParticle(n_child, m_pdgIdN2))
            {
              vec_mu_from_n2.push_back(n_child);
            }
            vec_mu.push_back(n_child);
            vec_lep.push_back(n_child);
          }

          if (MC::isTau(n_child->pdgId()) && !Truth::isFromPhoton(n_child))
          {
            // Skip if it is a self-decay
            if ((n_child->parent(0)->pdgId() == n_child->pdgId()))
              continue;

            if (Truth::isDirectlyFromParticle(n_child, m_pdgIdN1))
            {
              ANA_MSG_DEBUG("Tau from N1!!!");
              vec_tau_from_n1.push_back(n_child);
              tauFromN1++;
            }
            else if (Truth::isDirectlyFromParticle(n_child, m_pdgIdN2))
            {
              ANA_MSG_DEBUG("Tau from N2!!!");
              vec_tau_from_n2.push_back(n_child);
              tauFromN2++;
            }
            else if (Truth::isDirectlyFromParticle(n_child, m_pdgIdN3))
            {
              vec_tau_from_n3.push_back(n_child);
            }
            vec_tau.push_back(n_child);
            vec_lep.push_back(n_child);
          }

          if (MC::isQuark(n_child->pdgId()) && !Truth::isFromGluon(n_child))
          {
            // Skip if the particle falls out of the detector acceptance if requested by m_detector
            if (m_detector && !Truth::passDetector(n_child, 20., 4.9, true))
              continue;

            vec_q.push_back(n_child);
          }

          sumljj += Truth::getLV(n_child);
          sum2l4j += Truth::getLV(n_child);
        }
        NDecayProducts.push_back(sumljj.M() / Truth::GeV);
      }
      ddDecayProducts2l4j.push_back(sum2l4j.M() / Truth::GeV);
      ddDecayProductsNN.push_back(sumNN.M() / Truth::GeV);
    }
  }

  // Count events that do not contain mediating Delta
  if (vec_dd.size() < 1)
  {
    ANA_MSG_DEBUG("Event " << eventInfo->eventNumber() << " without Deltas.");
    noDD++;
  }

  // Sort lepton vectors by their pts
  std::sort(vec_el.begin(), vec_el.end(), Truth::compareByPt);
  std::sort(vec_mu.begin(), vec_mu.end(), Truth::compareByPt);
  std::sort(vec_tau.begin(), vec_tau.end(), Truth::compareByPt);
  std::sort(vec_lep.begin(), vec_lep.end(), Truth::compareByPt);

  hist("h_N_dd")->Fill(vec_dd.size());
  hist("h_N_mediators")->Fill(vec_mediators.size());
  hist("h_N_n1")->Fill(vec_n1.size());
  hist("h_N_n2")->Fill(vec_n2.size());
  hist("h_N_n3")->Fill(vec_n3.size());

  unsigned int mctcResult{};
  for (auto dd : vec_dd)
  {
    hist("h_m_dd")->Fill(dd->m() / Truth::GeV);
    hist("h_pdgid_dd")->Fill(dd->pdgId());
    if (m_mctcAccessor->isAvailable(*dd))
    {
      mctcResult = (*m_mctcAccessor)(*dd);
    }
    Truth::fillMCTC(hist2d("h_mctc_dd"), dd, mctcResult);
  }
  for (auto med : vec_mediators)
  {
    hist("h_pdgid_mediators")->Fill(med->pdgId());
  }
  for (auto N1 : vec_n1)
  {
    hist("h_m_n1")->Fill(N1->m() / Truth::GeV);
    hist("h_pdgid_n1")->Fill(N1->pdgId());
    if (m_mctcAccessor->isAvailable(*N1))
    {
      mctcResult = (*m_mctcAccessor)(*N1);
    }
    Truth::fillMCTC(hist2d("h_mctc_n1"), N1, mctcResult);
  }
  for (auto N2 : vec_n2)
  {
    hist("h_m_n2")->Fill(N2->m() / Truth::GeV);
    hist("h_pdgid_n2")->Fill(N2->pdgId());
    if (m_mctcAccessor->isAvailable(*N2))
    {
      mctcResult = (*m_mctcAccessor)(*N2);
    }
    Truth::fillMCTC(hist2d("h_mctc_n2"), N2, mctcResult);
  }
  for (auto N3 : vec_n3)
  {
    hist("h_m_n3")->Fill(N3->m() / Truth::GeV);
    hist("h_pdgid_n3")->Fill(N3->pdgId());
    if (m_mctcAccessor->isAvailable(*N3))
    {
      mctcResult = (*m_mctcAccessor)(*N3);
    }
    Truth::fillMCTC(hist2d("h_mctc_n3"), N3, mctcResult);
  }

  if (vec_n1.size() >= 2)
  {
    hist("h_dR_nn")->Fill(DeltaR(vec_n1.at(0)->p4(), vec_n1.at(1)->p4()));
  }
  else if (vec_n2.size() >= 2)
  {
    hist("h_dR_nn")->Fill(DeltaR(vec_n2.at(0)->p4(), vec_n2.at(1)->p4()));
  }
  else if (vec_n3.size() >= 2)
  {
    hist("h_dR_nn")->Fill(DeltaR(vec_n3.at(0)->p4(), vec_n3.at(1)->p4()));
  }

  // dd decay products NN
  for (auto dec : ddDecayProductsNN)
  {
    hist("h_m_ddDecayProductsNN")->Fill(dec);
  }
  // dd decay products 2l4j
  for (auto dec : ddDecayProducts2l4j)
  {
    hist("h_m_ddDecayProducts2l4j")->Fill(dec);
  }
  // N decay products
  for (auto dec : NDecayProducts)
  {
    hist("h_m_nDecayProducts")->Fill(dec);
  }
  hist("h_N_el_from_n")->Fill(vec_el.size());
  hist("h_N_el_from_n1")->Fill(vec_el_from_n1.size());
  for (auto el : vec_el)
  {
    hist("h_pt_el")->Fill(el->pt() / Truth::GeV);
    hist("h_eta_el")->Fill(el->eta());
    hist("h_phi_el")->Fill(el->phi());
    if (m_mctcAccessor->isAvailable(*el))
    {
      mctcResult = (*m_mctcAccessor)(*el);
    }
    Truth::fillMCTC(hist2d("h_mctc_el"), el, mctcResult);
  }
  if (vec_el.size() >= 1)
  {
    hist("h_pt_el_lead")->Fill(vec_el.at(0)->pt() / Truth::GeV);
    hist("h_eta_el_lead")->Fill(vec_el.at(0)->eta());
    hist("h_phi_el_lead")->Fill(vec_el.at(0)->phi());
    if (vec_el.size() >= 2)
    {
      hist("h_pt_el_sublead")->Fill(vec_el.at(1)->pt() / Truth::GeV);
      hist("h_eta_el_sublead")->Fill(vec_el.at(1)->eta());
      hist("h_phi_el_sublead")->Fill(vec_el.at(1)->phi());
      hist("h_dR_elel")->Fill(DeltaR(vec_el.at(0)->p4(), vec_el.at(1)->p4()));
    }
  }

  hist("h_N_mu_from_n")->Fill(vec_mu.size());
  hist("h_N_mu_from_n2")->Fill(vec_mu_from_n2.size());
  for (auto mu : vec_mu)
  {
    hist("h_pt_mu")->Fill(mu->pt() / Truth::GeV);
    hist("h_eta_mu")->Fill(mu->eta());
    hist("h_phi_mu")->Fill(mu->phi());
    if (m_mctcAccessor->isAvailable(*mu))
    {
      mctcResult = (*m_mctcAccessor)(*mu);
    }
    Truth::fillMCTC(hist2d("h_mctc_mu"), mu, mctcResult);
  }
  if (vec_mu.size() >= 1)
  {
    hist("h_pt_mu_lead")->Fill(vec_mu.at(0)->pt() / Truth::GeV);
    hist("h_eta_mu_lead")->Fill(vec_mu.at(0)->eta());
    hist("h_phi_mu_lead")->Fill(vec_mu.at(0)->phi());
    if (vec_mu.size() >= 2)
    {
      hist("h_pt_mu_sublead")->Fill(vec_mu.at(1)->pt() / Truth::GeV);
      hist("h_eta_mu_sublead")->Fill(vec_mu.at(1)->eta());
      hist("h_phi_mu_sublead")->Fill(vec_mu.at(1)->phi());
      hist("h_dR_mumu")->Fill(DeltaR(vec_mu.at(0)->p4(), vec_mu.at(1)->p4()));
    }
  }

  hist("h_N_tau_from_n")->Fill(vec_tau.size());
  hist("h_N_tau_from_n3")->Fill(vec_tau_from_n3.size());
  for (auto tau : vec_tau)
  {
    hist("h_pt_tau")->Fill(tau->pt() / Truth::GeV);
    hist("h_eta_tau")->Fill(tau->eta());
    hist("h_phi_tau")->Fill(tau->phi());
    if (m_mctcAccessor->isAvailable(*tau))
    {
      mctcResult = (*m_mctcAccessor)(*tau);
    }
    Truth::fillMCTC(hist2d("h_mctc_tau"), tau, mctcResult);
  }
  if (vec_tau.size() >= 1)
  {
    hist("h_pt_tau_lead")->Fill(vec_tau.at(0)->pt() / Truth::GeV);
    hist("h_eta_tau_lead")->Fill(vec_tau.at(0)->eta());
    hist("h_phi_tau_lead")->Fill(vec_tau.at(0)->phi());
    if (vec_tau.size() >= 2)
    {
      hist("h_pt_tau_sublead")->Fill(vec_tau.at(1)->pt() / Truth::GeV);
      hist("h_eta_tau_sublead")->Fill(vec_tau.at(1)->eta());
      hist("h_phi_tau_sublead")->Fill(vec_tau.at(1)->phi());
      hist("h_dR_tautau")->Fill(DeltaR(vec_tau.at(0)->p4(), vec_tau.at(1)->p4()));
    }
  }

  for (auto displacement : vec_displacement)
  {
    hist("h_displacement")->Fill(displacement.Mag());
    hist("h_displacement_trans")->Fill(displacement.Perp());
    hist("h_displacement_long")->Fill(displacement.Z() * std::sin(displacement.Theta()));
  }

  hist2d("h_N_el_vs_N_mu")->Fill(vec_el.size(), vec_mu.size());
  hist2d("h_N_el_vs_N_tau")->Fill(vec_el.size(), vec_tau.size());
  hist2d("h_N_mu_vs_N_tau")->Fill(vec_mu.size(), vec_tau.size());
  if (vec_n1.size() >= 2)
  {
    hist2d("h_N_eln1_vs_N_mun1")->Fill(vec_el_from_n1.size(), vec_mu_from_n1.size());
    hist2d("h_N_eln1_vs_N_taun1")->Fill(vec_el_from_n1.size(), vec_tau_from_n1.size());
    hist2d("h_N_mun1_vs_N_taun1")->Fill(vec_mu_from_n1.size(), vec_tau_from_n1.size());
  }
  if (vec_n2.size() >= 2)
  {
    hist2d("h_N_eln2_vs_N_mun2")->Fill(vec_el_from_n2.size(), vec_mu_from_n2.size());
    hist2d("h_N_eln2_vs_N_taun2")->Fill(vec_el_from_n2.size(), vec_tau_from_n2.size());
    hist2d("h_N_mun2_vs_N_taun2")->Fill(vec_mu_from_n2.size(), vec_tau_from_n2.size());
  }
  if (vec_n3.size() >= 2)
  {
    hist2d("h_N_eln3_vs_N_mun3")->Fill(vec_el_from_n3.size(), vec_mu_from_n3.size());
    hist2d("h_N_eln3_vs_N_taun3")->Fill(vec_el_from_n3.size(), vec_tau_from_n3.size());
    hist2d("h_N_mun3_vs_N_taun3")->Fill(vec_mu_from_n3.size(), vec_tau_from_n3.size());
  }

  hist("h_N_lep_from_n")->Fill(vec_lep.size());
  for (auto lep : vec_lep)
  {
    hist("h_pdgid_lep")->Fill(lep->pdgId());
    hist("h_pt_lep")->Fill(lep->pt() / Truth::GeV);
    hist("h_eta_lep")->Fill(lep->eta());
    hist("h_phi_lep")->Fill(lep->phi());
  }
  if (vec_lep.size() >= 2)
  {
    hist("h_pt_lep_lead")->Fill(vec_lep.at(0)->pt() / Truth::GeV);
    hist("h_eta_lep_lead")->Fill(vec_lep.at(0)->eta());
    hist("h_phi_lep_lead")->Fill(vec_lep.at(0)->phi());
    hist("h_pt_lep_sublead")->Fill(vec_lep.at(1)->pt() / Truth::GeV);
    hist("h_eta_lep_sublead")->Fill(vec_lep.at(1)->eta());
    hist("h_phi_lep_sublead")->Fill(vec_lep.at(1)->phi());
    hist("h_dR_leplep")->Fill(DeltaR(vec_lep.at(0)->p4(), vec_lep.at(1)->p4()));
    hist("h_pTratio_lep")->Fill(vec_lep.at(1)->pt() / vec_lep.at(0)->pt());
  }

  hist("h_N_q_hns")->Fill(vec_q.size());
  for (auto q : vec_q)
  {
    hist("h_pdgid_q")->Fill(q->pdgId());
    hist("h_pt_q")->Fill(q->pt() / Truth::GeV);
    hist("h_eta_q")->Fill(q->eta());
    hist("h_phi_q")->Fill(q->phi());
  }

  m_dd_pt->clear();
  m_dd_eta->clear();
  m_dd_phi->clear();
  m_dd_m->clear();
  m_dd_pdgid->clear();

  for (auto dd : vec_dd)
  {
    m_dd_pt->push_back(dd->pt());
    m_dd_eta->push_back(dd->eta());
    m_dd_phi->push_back(dd->phi());
    m_dd_m->push_back(dd->m());
    m_dd_pdgid->push_back(dd->pdgId());
  }

  m_n1_pt->clear();
  m_n1_eta->clear();
  m_n1_phi->clear();
  m_n1_m->clear();
  m_n1_pdgid->clear();

  for (auto n1 : vec_n1)
  {
    m_n1_pt->push_back(n1->pt());
    m_n1_eta->push_back(n1->eta());
    m_n1_phi->push_back(n1->phi());
    m_n1_m->push_back(n1->m());
    m_n1_pdgid->push_back(n1->pdgId());
  }

  m_n2_pt->clear();
  m_n2_eta->clear();
  m_n2_phi->clear();
  m_n2_m->clear();
  m_n2_pdgid->clear();

  for (auto n2 : vec_n2)
  {
    m_n2_pt->push_back(n2->pt());
    m_n2_eta->push_back(n2->eta());
    m_n2_phi->push_back(n2->phi());
    m_n2_m->push_back(n2->m());
    m_n2_pdgid->push_back(n2->pdgId());
  }

  m_n3_pt->clear();
  m_n3_eta->clear();
  m_n3_phi->clear();
  m_n3_m->clear();
  m_n3_pdgid->clear();

  for (auto n3 : vec_n3)
  {
    m_n3_pt->push_back(n3->pt());
    m_n3_eta->push_back(n3->eta());
    m_n3_phi->push_back(n3->phi());
    m_n3_m->push_back(n3->m());
    m_n3_pdgid->push_back(n3->pdgId());
  }

  m_lep_pt->clear();
  m_lep_eta->clear();
  m_lep_phi->clear();
  m_lep_m->clear();
  m_lep_pdgid->clear();

  for (auto lep : vec_lep)
  {
    m_lep_pt->push_back(lep->pt());
    m_lep_eta->push_back(lep->eta());
    m_lep_phi->push_back(lep->phi());
    m_lep_m->push_back(lep->m());
    m_lep_pdgid->push_back(lep->pdgId());
  }

  m_el_pt->clear();
  m_el_eta->clear();
  m_el_phi->clear();
  m_el_m->clear();

  for (auto el : vec_el)
  {
    m_el_pt->push_back(el->pt());
    m_el_eta->push_back(el->eta());
    m_el_phi->push_back(el->phi());
    m_el_m->push_back(el->m());
  }

  m_mu_pt->clear();
  m_mu_eta->clear();
  m_mu_phi->clear();
  m_mu_m->clear();

  for (auto mu : vec_mu)
  {
    m_mu_pt->push_back(mu->pt());
    m_mu_eta->push_back(mu->eta());
    m_mu_phi->push_back(mu->phi());
    m_mu_m->push_back(mu->m());
  }

  m_tau_pt->clear();
  m_tau_eta->clear();
  m_tau_phi->clear();
  m_tau_m->clear();

  for (auto tau : vec_tau)
  {
    m_tau_pt->push_back(tau->pt());
    m_tau_eta->push_back(tau->eta());
    m_tau_phi->push_back(tau->phi());
    m_tau_m->push_back(tau->m());
  }

  m_q_pt->clear();
  m_q_eta->clear();
  m_q_phi->clear();
  m_q_m->clear();
  m_q_pdgid->clear();

  for (auto q : vec_q)
  {
    m_q_pt->push_back(q->pt());
    m_q_eta->push_back(q->eta());
    m_q_phi->push_back(q->phi());
    m_q_m->push_back(q->m());
    m_q_pdgid->push_back(q->pdgId());
  }

  // Jets
  const xAOD::JetContainer *truth_jets = nullptr;
  ANA_CHECK(evtStore()->retrieve(truth_jets, "AntiKt4TruthDressedWZJets"));

  m_jet_pt->clear();
  m_jet_eta->clear();
  m_jet_phi->clear();
  m_jet_m->clear();

  int n_jets = 0;
  for (auto jet : *truth_jets)
  {
    // Skip jets with pt < 20 GeV
    if (jet->pt() / Truth::GeV < 20.)
      continue;

    m_jet_pt->push_back(jet->pt());
    m_jet_eta->push_back(jet->eta());
    m_jet_phi->push_back(jet->phi());
    m_jet_m->push_back(jet->m());

    hist("h_pt_jet")->Fill(jet->pt() / Truth::GeV);
    hist("h_eta_jet")->Fill(jet->eta());
    hist("h_phi_jet")->Fill(jet->phi());

    vec_jet_pt.push_back(jet->pt() / Truth::GeV);
    n_jets++;
  }
  hist("h_N_jet")->Fill(n_jets);

  std::sort(vec_jet_pt.begin(), vec_jet_pt.end(), std::greater<>());
  if (!vec_jet_pt.empty())
  {
    hist("h_pt_jet_lead")->Fill(vec_jet_pt.at(0));

    if (vec_jet_pt.size() >= 2)
    {
      hist("h_pt_jet_sublead")->Fill(vec_jet_pt.at(1));
      hist2d("h_ptjet1_vs_ptjet2")->Fill(vec_jet_pt.at(0), vec_jet_pt.at(1));

      // Calculate sum of subleading jets
      float sum_subjet_pt = std::accumulate(vec_jet_pt.begin() + 1, vec_jet_pt.end(), 0.);
      hist("h_pTratio_jet")->Fill(sum_subjet_pt / vec_jet_pt.at(0));
    }

    // Fill additional jet histograms if more jets exist
    if (vec_jet_pt.size() >= 3)
    {
      hist("h_pt_jet_3")->Fill(vec_jet_pt.at(2));
    }
    if (vec_jet_pt.size() >= 4)
    {
      hist("h_pt_jet_4")->Fill(vec_jet_pt.at(3));
    }
  }

  // MET
  const xAOD::MissingETContainer *truth_met = nullptr;
  ANA_CHECK(evtStore()->retrieve(truth_met, "MET_Truth"));

  for (auto met : *truth_met)
  {
    if (met->name() == "NonInt")
    {
      m_met = met->met();
    }
  }

  hist("h_met")->Fill(m_met / Truth::GeV);

  tree("analysis")->Fill();

  if (m_eventNumber > 0 && m_eventNumber % 1000 == 0)
  {
    ANA_MSG_INFO("Done " << m_eventNumber << " events.");
  }
  m_eventNumber++;
  return StatusCode::SUCCESS;
}

StatusCode MCValAlgHNs::finalize()
{
  ANA_MSG_INFO("================== SUMMARY =================");
  ANA_MSG_INFO("Number of electrons directly from N2: " << elFromN2);
  ANA_MSG_INFO("Number of electrons directly from N3: " << elFromN3);
  ANA_MSG_INFO("Number of muons directly from N1: " << muFromN1);
  ANA_MSG_INFO("Number of muons directly from N3: " << muFromN3);
  ANA_MSG_INFO("Number of taus directly from N1: " << tauFromN1);
  ANA_MSG_INFO("Number of taus directly from N2: " << tauFromN2);
  ANA_MSG_INFO("Events without Deltas: " << noDD);
  ANA_MSG_INFO("============== END OF SUMMARY ==============");

  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk. This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

MCValAlgHNs::~MCValAlgHNs()
{

  // Delete the allocated vectors to avoid memory leaks
  delete m_dd_pt;
  delete m_dd_eta;
  delete m_dd_phi;
  delete m_dd_m;
  delete m_dd_pdgid;

  delete m_n1_pt;
  delete m_n1_eta;
  delete m_n1_phi;
  delete m_n1_m;
  delete m_n1_pdgid;

  delete m_n2_pt;
  delete m_n2_eta;
  delete m_n2_phi;
  delete m_n2_m;
  delete m_n2_pdgid;

  delete m_n3_pt;
  delete m_n3_eta;
  delete m_n3_phi;
  delete m_n3_m;
  delete m_n3_pdgid;

  delete m_lep_pt;
  delete m_lep_eta;
  delete m_lep_phi;
  delete m_lep_m;
  delete m_lep_pdgid;

  delete m_el_pt;
  delete m_el_eta;
  delete m_el_phi;
  delete m_el_m;

  delete m_mu_pt;
  delete m_mu_eta;
  delete m_mu_phi;
  delete m_mu_m;

  delete m_tau_pt;
  delete m_tau_eta;
  delete m_tau_phi;
  delete m_tau_m;

  delete m_q_pt;
  delete m_q_eta;
  delete m_q_phi;
  delete m_q_m;
  delete m_q_pdgid;

  delete m_jet_pt;
  delete m_jet_eta;
  delete m_jet_phi;
  delete m_jet_m;
}
