#include <filesystem>
namespace fs = std::filesystem;

#include <TH1.h>
#include <TKey.h>
#include <TFile.h>
#include <TError.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <THStack.h>

#include "Plot/Colors.h"
#include "Plot/AtlasStyle.h"
#include "Plot/PlotUtils.h"

// Define which variables should have any logarithmic axes
std::vector<PlotUtils::logVariable> logVariables = {{"h_displacement", true, true}, {"h_displacement_trans", true, true}, {"h_displacement_long", false, true}};

// Main plotting function
void plot(std::string histoPath, std::string type, std::string runDir, std::string plotOnlyDSID, std::string plotOnlyFormat)
{
    SetAtlasStyle();
    Color::setup();

    // Rebining for HNs pt range
    int i = 0;
    double pt_range[50];
    for (int j = 0; j < 100; j += 2)
    {
        pt_range[i] = j;
        i++;
    }

    // Read in file paths
    std::vector<fs::path> files;
    std::vector<std::string> formats;
    bool isFromGridpack = histoPath.ends_with("GRIDPACKS");
    std::string outputType = isFromGridpack ? fs::path(histoPath).parent_path().filename() : fs::path(histoPath).filename();
    std::string minDSID = PlotUtils::extractSubstring(plotOnlyDSID, '-', 0);
    std::string maxDSID = PlotUtils::extractSubstring(plotOnlyDSID, '-', 1);
    for (const auto &entry : fs::directory_iterator(histoPath))
    {
        if (entry.path().filename().string().compare("GRIDPACKS") == 0 || entry.path().extension().string().compare(".pdf") == 0)
        {
            continue;
        }
        int dsid = std::stoi(PlotUtils::extractSubstring(entry.path().filename().string(), '_', 2));
        std::string format = PlotUtils::extractSubstring(entry.path().filename().string(), '_', 1);
        if (format.find("TRUTH") == std::string::npos && format.find("PHYS") == std::string::npos)
        {
            std::cerr << "Wrong input folder. Should contain the TRUTH or PHYS keyword! Exiting..." << std::endl;
            return;
        }
        if (entry.path().string().find("DAOD_") != std::string::npos)
        {
            if (plotOnlyFormat.empty() || plotOnlyFormat.compare(format) == 0)
            {
                if (plotOnlyDSID.empty() || (dsid >= std::stoi(minDSID) && dsid <= std::stoi(maxDSID)))
                {
                    files.push_back(entry.path());
                }
                if (std::find(formats.begin(), formats.end(), format) == std::end(formats))
                {
                    formats.push_back(format);
                }
            }
        }
    }

    char esc_char = 27; // the decimal code for escape character
    sort(files.begin(), files.end());
    int nFiles = files.size() / formats.size();
    for (const std::string &format : formats)
    {
        std::cout << "Running on " << format << " format:" << std::endl;

        // Open first file as a reference
        auto it = std::find_if(files.begin(), files.end(), [format](const std::string &name)
                               { return name.find(format) != std::string::npos; });
        size_t idx = std::distance(files.begin(), it);
        std::string referenceDsid = PlotUtils::extractSubstring(files[idx].filename().string(), '_', 2);
        std::string referenceName = files[idx].string() + "/hist-" + referenceDsid + ".root";
        TFile *referenceFile = TFile::Open((referenceName).c_str(), "READ");
        if (!referenceFile || referenceFile->IsZombie())
        {
            std::cerr << "Could not open reference file: " << referenceName << std::endl;
            return;
        }

        int histCounter = 1;
        int nEntries = gDirectory->GetListOfKeys()->GetEntries() - 5;
        TIter keyList(gDirectory->GetListOfKeys());
        TKey *key;
        while ((key = (TKey *)keyList()))
        {
            TClass *cl = gROOT->GetClass(key->GetClassName());
            if (!cl->InheritsFrom("TH1"))
            {
                continue;
            }
            TH1 *hist = (TH1 *)key->ReadObj();
            std::string histName = hist->GetName();
            if (histName.find("h_") == std::string::npos)
            {
                std::cout << "Skipping branch: " << histName << std::endl;
                continue;
            }

            std::unique_ptr<THStack> outStack = std::make_unique<THStack>("stack", "");
            std::unique_ptr<TCanvas> canvas = std::make_unique<TCanvas>("canvas", histName.c_str());
            std::unique_ptr<TLegend> legend = std::make_unique<TLegend>(histName.find("_pt_") != std::string::npos ? 0.6 : 0.7, 0.25, 0.95, 0.85);

            int entries = 0;
            double alpha = 1.0;
            size_t lineStyle = 1;
            size_t colorIndex = 0;
            std::string mass, mN, mDD, dsid;
            bool is2Dplot = histName.find("_vs_") != std::string::npos || histName.find("_mctc_") != std::string::npos;

            for (unsigned int i = 0; i < files.size(); i++)
            {
                std::string parentDir = files[i].filename();
                dsid = PlotUtils::extractSubstring(parentDir, '_', 2);

                // Skip if other DAOD format
                if (parentDir.find(format) == std::string::npos)
                {
                    continue;
                }

                if (type.find("CASCADES") != std::string::npos || type.find("ALPS") != std::string::npos)
                {
                    int mass_pos = parentDir.find("m");
                    mass = parentDir.substr(mass_pos + 1);
                }
                else
                {
                    int N_pos = parentDir.find("N");
                    int D_pos = parentDir.find("DD");
                    mN = parentDir.substr(N_pos + 1, D_pos - N_pos - 2);
                    mDD = parentDir.substr(D_pos + 2);
                }

                std::string filename = files[i].string() + "/hist-" + dsid + ".root";
                TFile *file = TFile::Open(filename.c_str(), "READ");
                if (!file || file->IsZombie())
                {
                    std::cerr << "Could not open file: " << filename << std::endl;
                    continue;
                }
                TH1 *h = dynamic_cast<TH1 *>(file->Get(histName.c_str()));
                if (!h)
                {
                    std::cerr << "Could not retrieve histogram: " << histName << std::endl;
                    file->Close();
                    delete file;
                    continue;
                }

                if (type == "CASCADES" && (histName.find("_pt_") != std::string::npos || histName.find("_met") != std::string::npos))
                {
                    h = h->Rebin(10);
                }
                else if (type.find("HNS") != std::string::npos && (histName.find("_pt_") != std::string::npos || histName.find("_met") != std::string::npos))
                {
                    h = h->Rebin(49, (histName + "_aux").c_str(), pt_range);
                }

                // Prepare logarthmic axes
                PlotUtils::setupLogAxis(histName, logVariables, canvas.get(), h);

                h->SetLineWidth(2);
                h->SetLineColorAlpha(LINE_COLORS[colorIndex], alpha);
                h->SetLineStyle(lineStyle);
                colorIndex++;

                // Change color and line style if large number of histograms
                if (colorIndex >= LINE_COLORS.size())
                {
                    colorIndex = 0;
                    alpha *= 0.75;
                    lineStyle++;
                }

                // PlotUtils::processUnderflow(&h[0], false);
                // PlotUtils::processOverflow(&h[0], false);

                entries += h->GetEntries();
                outStack->Add(h);

                if (type == "CASCADES")
                {
                    legend->AddEntry(h, ("m_{#Delta^{#pm}} = " + mass + " GeV").c_str(), "l");
                }
                else if (type.find("HNS") != std::string::npos)
                {
                    if (histName.find("_pt_") != std::string::npos)
                    {
                        legend->AddEntry(h, "N_{" + mN + "} #Delta_{" + mDD + "}, N = " + h->GetEntries(), "l");
                    }
                    else
                    {
                        legend->AddEntry(h, ("N_{" + mN + "} #Delta_{" + mDD + "}").c_str(), "l");
                    }
                }
                else if (type == "ALPS")
                {
                    legend->AddEntry(h, ("m_{ALP} = " + mass + " GeV").c_str(), "l");
                }
                h->SetDirectory(0);
                file->Close();
                delete file;
            }

            int mult = (type == "CASCADES" || type == "ALPS") ? 1 : 2;
            std::cout << "\tProcessing histogram nr. " << histCounter << "/" << nEntries << ": " << esc_char << "[1m" << histName.substr(2) << esc_char << "[0m with " << mult * entries / files.size() << " entries in average." << std::endl;

            if (is2Dplot)
            {
                auto [nRows, nCols] = PlotUtils::findOptimalGrid(nFiles);
                canvas->SetCanvasSize(std::max(nCols * 300, 800), nRows * 300);
                PlotUtils::setup2Dplots(outStack.get(), canvas.get(), legend.get());
            }
            else
            {
                canvas->SetCanvasSize(800, 600);
                outStack->Draw("NOSTACK");
                outStack->GetXaxis()->SetTitle(Variables::getVar(histName).c_str());
                outStack->GetYaxis()->SetTitle("Number of events");

                // Legend costumisation
                PlotUtils::setupLegend(legend.get());
                legend->Draw();

                canvas->SetTopMargin(0.08);
                PlotUtils::setupATLASLabel(canvas->GetLeftMargin(), 0.94, 0.05);
            }
            outStack->SetMaximum(1.1 * outStack->GetMaximum("NOSTACK"));

            canvas->RedrawAxis();
            canvas->Draw();

            // Save histograms to validation_histograms_%TYPE%_%FORMAT%_%from_GRIDPACK%.pdf
            std::string outputName = "validation_histograms_" + outputType + "_" + format;
            outputName += isFromGridpack ? "_from_GRIDPACK.pdf" : ".pdf";

            if (histName.find("h_N_d0") != std::string::npos || histName.find("h_N_dd") != std::string::npos || histName.find("h_N_alp") != std::string::npos)
            {
                outputName.append("(");
            }
            if (histName.find("h_met") != std::string::npos || (histName.find("h_costh_mumu") != std::string::npos && type == "ALPS"))
            {
                outputName.append(")");
            }
            canvas->SaveAs((runDir + "/" + outputName).c_str());
            canvas->Close();

            histCounter++;
        }
        referenceFile->Close();
        delete referenceFile;
        std::cout << std::endl;
    }
}

int main(int argc, char **argv)
{
    gErrorIgnoreLevel = kWarning;

    std::string histoPath;
    bool isInputProvided = false;
    std::string plotOnlyDSID = "";
    std::string plotOnlyFormat = "";
    std::vector<std::string> args(argv + 1, argv + argc);
    std::string type = std::getenv("TYPE") ? std::getenv("TYPE") : "";
    std::string runDir = std::getenv("ANALYSIS_RUN_DIR") ? std::getenv("ANALYSIS_RUN_DIR") : "";

    if (type != "CASCADES" && type != "2HNS" && type != "4HNS" && type != "ALPS")
    {
        std::cerr << "Environmental variable TYPE not set. Export to CASCADES, 2HNS, 4HNS or ALPS!" << std::endl;
        return 1;
    }

    if (runDir.empty())
    {
        std::cerr << "Environmental variable ANALYSIS_RUN_DIR not set. Run the setup script first!" << std::endl;
        return 1;
    }

    for (auto i = args.begin(); i != args.end(); ++i)
    {

        if (*i == "-h" || *i == "--help")
        {
            std::cout << "Usage: plot [-i|--input-path] <input-path> [-f|--format] <format> [-d|--dsid] <minDSID-maxDSID>" << std::endl;
            return 0;
        }
        else if (*i == "-i" || *i == "--input-path")
        {
            histoPath = *++i;
            isInputProvided = true;
        }
        else if (*i == "-f" || *i == "--format")
        {
            plotOnlyFormat = *++i;
        }
        else if (*i == "-d" || *i == "--dsid")
        {
            plotOnlyDSID = *++i;
        }
    }

    if (!isInputProvided)
    {
        std::cerr << "Input path required! Use: plot [-i|--input-path] <input-path>" << std::endl;
        return 2;
    }

    plot(histoPath, type, runDir, plotOnlyDSID, plotOnlyFormat);
    return 0;
}
