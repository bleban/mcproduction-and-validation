#include "Colors.h"

void Color::setup()
{
    new TColor(Green, 154. / 255, 245. / 255, 164. / 255);  // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(Yellow, 255. / 255, 255. / 255, 179. / 255); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(Purple, 190. / 255, 186. / 255, 218. / 255); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(Red, 251. / 255, 128. / 255, 114. / 255);    // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(Blue, 128. / 255, 177. / 255, 211. / 255);   // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(Orange, 250. / 255, 176. / 255, 124. / 255); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(Pink, 228. / 255, 170. / 255, 208. / 255);   // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(Gray, 215. / 255, 215. / 255, 215. / 255);   // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

    new TColor(GreenDark, 116. / 255, 184. / 255, 124. / 255);  // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(YellowDark, 217. / 255, 222. / 255, 122. / 255); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(PurpleDark, 151. / 255, 132. / 255, 173. / 255); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(RedDark, 209. / 255, 106. / 255, 94. / 255);     // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(BlueDark, 100. / 255, 137. / 255, 163. / 255);   // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(OrangeDark, 207. / 255, 146. / 255, 103. / 255); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(PinkDark, 173. / 255, 130. / 255, 158. / 255);   // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(GrayDark, 200. / 255, 200. / 255, 200. / 255);   // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

    new TColor(GreenBright, 189. / 255, 255. / 255, 195. / 255);  // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(YellowBright, 255. / 255, 255. / 255, 200. / 255); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(PurpleBright, 220. / 255, 215. / 255, 252. / 255); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(RedBright, 251. / 255, 192. / 255, 192. / 255);    // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(BlueBright, 172. / 255, 204. / 255, 223. / 255);   // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(OrangeBright, 250. / 255, 187. / 255, 146. / 255); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(PinkBright, 255. / 255, 191. / 255, 233. / 255);   // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    new TColor(GrayBright, 235. / 255, 235. / 255, 235. / 255);   // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
}
