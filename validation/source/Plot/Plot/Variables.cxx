#include <iostream>

#include "Variables.h"

namespace Variables
{
    Var getEnum(std::string q)
    {
        if (q == "h_N_d0")
        {
            return N_d0;
        }
        if (q == "h_N_dp")
        {
            return N_dp;
        }
        if (q == "h_N_dpp")
        {
            return N_dpp;
        }
        if (q == "h_N_chi")
        {
            return N_chi;
        }
        if (q == "h_N_dd")
        {
            return N_dd;
        }
        if (q == "h_N_mediators")
        {
            return N_mediators;
        }
        if (q == "h_N_n1")
        {
            return N_n1;
        }
        if (q == "h_N_n2")
        {
            return N_n2;
        }
        if (q == "h_N_n3")
        {
            return N_n3;
        }
        if (q == "h_N_alp")
        {
            return N_alp;
        }
        if (q == "h_pdgid_d0")
        {
            return pdgid_d0;
        }
        if (q == "h_pdgid_dp")
        {
            return pdgid_dp;
        }
        if (q == "h_pdgid_dpp")
        {
            return pdgid_dpp;
        }
        if (q == "h_pdgid_chi")
        {
            return pdgid_chi;
        }
        if (q == "h_pdgid_dd")
        {
            return pdgid_dd;
        }
        if (q == "h_pdgid_mediators")
        {
            return pdgid_mediators;
        }
        if (q == "h_pdgid_n1")
        {
            return pdgid_n1;
        }
        if (q == "h_pdgid_n2")
        {
            return pdgid_n2;
        }
        if (q == "h_pdgid_n3")
        {
            return pdgid_n3;
        }
        if (q == "h_pdgid_alp")
        {
            return pdgid_alp;
        }
        if (q == "h_m_d0")
        {
            return m_d0;
        }
        if (q == "h_m_dp")
        {
            return m_dp;
        }
        if (q == "h_m_dpp")
        {
            return m_dpp;
        }
        if (q == "h_m_chi")
        {
            return m_chi;
        }
        if (q == "h_m_dd")
        {
            return m_dd;
        }
        if (q == "h_m_n1")
        {
            return m_n1;
        }
        if (q == "h_m_n2")
        {
            return m_n2;
        }
        if (q == "h_m_n3")
        {
            return m_n3;
        }
        if (q == "h_m_alp")
        {
            return m_alp;
        }
        if (q == "h_m_ddDecayProductsNN")
        {
            return m_ddDecayProductsNN;
        }
        if (q == "h_m_ddDecayProducts2l4j")
        {
            return m_ddDecayProducts2l4j;
        }
        if (q == "h_m_nDecayProducts")
        {
            return m_nDecayProducts;
        }
        if (q == "h_dR_nn")
        {
            return dR_nn;
        }
        if (q == "h_displacement")
        {
            return displacement;
        }
        if (q == "h_displacement_trans")
        {
            return displacement_trans;
        }
        if (q == "h_displacement_long")
        {
            return displacement_long;
        }
        if (q == "h_mctc_dd")
        {
            return mctc_dd;
        }
        if (q == "h_mctc_n1")
        {
            return mctc_n1;
        }
        if (q == "h_mctc_n2")
        {
            return mctc_n2;
        }
        if (q == "h_mctc_n3")
        {
            return mctc_n3;
        }
        if (q == "h_mctc_el")
        {
            return mctc_el;
        }
        if (q == "h_mctc_mu")
        {
            return mctc_mu;
        }
        if (q == "h_mctc_tau")
        {
            return mctc_tau;
        }
        if (q == "h_mctc_alp")
        {
            return mctc_alp;
        }
        if (q == "h_N_el_cascades")
        {
            return N_el_cascades;
        }
        if (q == "h_N_el_from_n")
        {
            return N_el_from_n;
        }
        if (q == "h_N_el_from_n1")
        {
            return N_el_from_n1;
        }
        if (q == "h_N_el_alps")
        {
            return N_el_alps;
        }
        if (q == "h_N_el_other")
        {
            return N_el_other;
        }
        if (q == "h_pt_el")
        {
            return pt_el;
        }
        if (q == "h_pt_el_lead")
        {
            return pt_el_lead;
        }
        if (q == "h_pt_el_sublead")
        {
            return pt_el_sublead;
        }
        if (q == "h_eta_el")
        {
            return eta_el;
        }
        if (q == "h_eta_el_lead")
        {
            return eta_el_lead;
        }
        if (q == "h_eta_el_sublead")
        {
            return eta_el_sublead;
        }
        if (q == "h_phi_el")
        {
            return phi_el;
        }
        if (q == "h_phi_el_lead")
        {
            return phi_el_lead;
        }
        if (q == "h_phi_el_sublead")
        {
            return phi_el_sublead;
        }
        if (q == "h_dR_elel")
        {
            return dR_elel;
        }
        if (q == "h_N_mu_cascades")
        {
            return N_mu_cascades;
        }
        if (q == "h_N_mu_from_n")
        {
            return N_mu_from_n;
        }
        if (q == "h_N_mu_from_n2")
        {
            return N_mu_from_n2;
        }
        if (q == "h_N_mu_alps")
        {
            return N_mu_alps;
        }
        if (q == "h_N_mu_other")
        {
            return N_mu_other;
        }
        if (q == "h_pt_mu")
        {
            return pt_mu;
        }
        if (q == "h_pt_mu_lead")
        {
            return pt_mu_lead;
        }
        if (q == "h_pt_mu_sublead")
        {
            return pt_mu_sublead;
        }
        if (q == "h_eta_mu")
        {
            return eta_mu;
        }
        if (q == "h_eta_mu_lead")
        {
            return eta_mu_lead;
        }
        if (q == "h_eta_mu_sublead")
        {
            return eta_mu_sublead;
        }
        if (q == "h_phi_mu")
        {
            return phi_mu;
        }
        if (q == "h_phi_mu_lead")
        {
            return phi_mu_lead;
        }
        if (q == "h_phi_mu_sublead")
        {
            return phi_mu_sublead;
        }
        if (q == "h_m_mumu")
        {
            return m_mumu;
        }
        if (q == "h_dR_mumu")
        {
            return dR_mumu;
        }
        if (q == "h_aco_mumu")
        {
            return aco_mumu;
        }
        if (q == "h_costh_mumu")
        {
            return costh_mumu;
        }
        if (q == "h_N_tau_cascades")
        {
            return N_tau_cascades;
        }
        if (q == "h_N_tau_from_n")
        {
            return N_tau_from_n;
        }
        if (q == "h_N_tau_from_n3")
        {
            return N_tau_from_n3;
        }
        if (q == "h_N_tau_alps")
        {
            return N_tau_alps;
        }
        if (q == "h_N_tau_other")
        {
            return N_tau_other;
        }
        if (q == "h_pt_tau")
        {
            return pt_tau;
        }
        if (q == "h_pt_tau_lead")
        {
            return pt_tau_lead;
        }
        if (q == "h_pt_tau_sublead")
        {
            return pt_tau_sublead;
        }
        if (q == "h_eta_tau")
        {
            return eta_tau;
        }
        if (q == "h_eta_tau_lead")
        {
            return eta_tau_lead;
        }
        if (q == "h_eta_tau_sublead")
        {
            return eta_tau_sublead;
        }
        if (q == "h_phi_tau")
        {
            return phi_tau;
        }
        if (q == "h_phi_tau_lead")
        {
            return phi_tau_lead;
        }
        if (q == "h_phi_tau_sublead")
        {
            return phi_tau_sublead;
        }
        if (q == "h_dR_tautau")
        {
            return dR_tautau;
        }
        if (q == "h_N_lep_cascades")
        {
            return N_lep_cascades;
        }
        if (q == "h_N_lep_from_n")
        {
            return N_lep_from_n;
        }
        if (q == "h_N_lep_other")
        {
            return N_lep_other;
        }
        if (q == "h_pdgid_lep")
        {
            return pdgid_lep;
        }
        if (q == "h_pt_lep")
        {
            return pt_lep;
        }
        if (q == "h_pt_lep_lead")
        {
            return pt_lep_lead;
        }
        if (q == "h_pt_lep_sublead")
        {
            return pt_lep_sublead;
        }
        if (q == "h_eta_lep")
        {
            return eta_lep;
        }
        if (q == "h_eta_lep_lead")
        {
            return eta_lep_lead;
        }
        if (q == "h_eta_lep_sublead")
        {
            return eta_lep_sublead;
        }
        if (q == "h_phi_lep")
        {
            return phi_lep;
        }
        if (q == "h_phi_lep_lead")
        {
            return phi_lep_lead;
        }
        if (q == "h_phi_lep_sublead")
        {
            return phi_lep_sublead;
        }
        if (q == "h_dR_leplep")
        {
            return dR_leplep;
        }
        if (q == "h_pTratio_lep")
        {
            return pTratio_lep;
        }
        if (q == "h_N_q_cascades")
        {
            return N_q_cascades;
        }
        if (q == "h_N_q_hns")
        {
            return N_q_hns;
        }
        if (q == "h_N_q_other")
        {
            return N_q_other;
        }
        if (q == "h_pdgid_q")
        {
            return pdgid_q;
        }
        if (q == "h_pt_q")
        {
            return pt_q;
        }
        if (q == "h_eta_q")
        {
            return eta_q;
        }
        if (q == "h_phi_q")
        {
            return phi_q;
        }
        if (q == "h_N_jet")
        {
            return N_jet;
        }
        if (q == "h_pt_jet")
        {
            return pt_jet;
        }
        if (q == "h_pTratio_jet")
        {
            return pTratio_jet;
        }
        if (q == "h_pt_jet_lead")
        {
            return pt_jet_lead;
        }
        if (q == "h_pt_jet_sublead")
        {
            return pt_jet_sublead;
        }
        if (q == "h_pt_jet_3")
        {
            return pt_jet_3;
        }
        if (q == "h_pt_jet_4")
        {
            return pt_jet_4;
        }
        if (q == "h_eta_jet")
        {
            return eta_jet;
        }
        if (q == "h_phi_jet")
        {
            return phi_jet;
        }
        if (q == "h_met")
        {
            return met;
        }
        abort();
    }

    std::string getVar(const std::string histName)
    {
        std::string value;
        Var q = getEnum(histName);

        switch (q)
        {
        case N_d0:
            return "N(#Delta^{0}_{44})";
        case N_dp:
            return "N(#Delta^{#pm}_{38})";
        case N_dpp:
            return "N(#Delta^{#pm#pm}_{61})";
        case N_chi:
            return "N(#chi_{62})";
        case N_dd:
            return "N(#Delta)";
        case N_mediators:
            return "N(other mediators)";
        case N_n1:
            return "N(^{}N_{1})";
        case N_n2:
            return "N(^{}N_{2})";
        case N_n3:
            return "N(^{}N_{3})";
        case N_alp:
            return "N(ALP)";
        case pdgid_d0:
            return "PDGID(#Delta^{0}_{44})";
        case pdgid_dp:
            return "PDGID(#Delta^{#pm}_{38})";
        case pdgid_dpp:
            return "PDGID(#Delta^{#pm#pm}_{61})";
        case pdgid_chi:
            return "PDGID(#chi_{62})";
        case pdgid_dd:
            return "PDGID(#Delta_{45})";
        case pdgid_mediators:
            return "PDGID(other mediators)";
        case pdgid_n1:
            return "PDGID(^{}N_{1})";
        case pdgid_n2:
            return "PDGID(^{}N_{2})";
        case pdgid_n3:
            return "PDGID(^{}N_{3})";
        case pdgid_alp:
            return "PDGID(ALP) - 9000000";
        case m_d0:
            return "m(#Delta^{0}_{44}) [GeV]";
        case m_dp:
            return "m(#Delta^{#pm}_{38}) [GeV]";
        case m_dpp:
            return "m(#Delta^{#pm#pm}_{61}) [GeV]";
        case m_chi:
            return "m(#chi_{62}) [GeV]";
        case m_dd:
            return "m(#Delta) [GeV]";
        case m_n1:
            return "m(^{}N_{1}) [GeV]";
        case m_n2:
            return "m(^{}N_{2}) [GeV]";
        case m_n3:
            return "m(^{}N_{3}) [GeV]";
        case m_alp:
            return "m(ALP) [GeV]";
        case m_ddDecayProductsNN:
            return "m(#Delta #rightarrow NN) [GeV]";
        case m_ddDecayProducts2l4j:
            return "m(#Delta #rightarrow 2l4j) [GeV]";
        case m_nDecayProducts:
            return "m(N #rightarrow ljj) [GeV]";
        case dR_nn:
            return "#DeltaR(NN)";
        case displacement:
            return "d_{vtx}(l) [mm]";
        case displacement_trans:
            return "d_{0}(l) [mm]";
        case displacement_long:
            return "z_{0} sin #theta (l) [mm]";
        case mctc_dd:
            return "MCTC(#Delta)";
        case mctc_n1:
            return "MCTC(^{}N_{1})";
        case mctc_n2:
            return "MCTC(^{}N_{2})";
        case mctc_n3:
            return "MCTC(^{}N_{3})";
        case mctc_el:
            return "MCTC(e)";
        case mctc_mu:
            return "MCTC(#mu)";
        case mctc_tau:
            return "MCTC(#tau)";
        case mctc_alp:
            return "MCTC(ALP)";
        case N_el_cascades:
            return "N(e) from #Delta^{#pm #pm}_{61}";
        case N_el_from_n:
            return "N(e) from N";
        case N_el_from_n1:
            return "N(e) from ^{}N_{1}";
        case N_el_alps:
            return "N(e) from ALP";
        case pt_el:
            return "p_{T}(e) [GeV]";
        case pt_el_lead:
            return "p_{T}(e)_{lead.} [GeV]";
        case pt_el_sublead:
            return "p_{T}(e)_{sublead.} [GeV]";
        case eta_el:
            return "#eta(e)";
        case eta_el_lead:
            return "#eta(e)_{lead.}";
        case eta_el_sublead:
            return "#eta(e)_{sublead.}";
        case phi_el:
            return "#varphi(e)";
        case phi_el_lead:
            return "#varphi(e)_{lead.}";
        case phi_el_sublead:
            return "#varphi(e)_{sublead.}";
        case dR_elel:
            return "#DeltaR(ee)";
        case N_el_other:
            return "N(e) other";
        case N_mu_cascades:
            return "N(#mu) from #Delta^{#pm#pm}_{61}";
        case N_mu_from_n:
            return "N(#mu) from N";
        case N_mu_from_n2:
            return "N(#mu) from ^{}N_{2}";
        case N_mu_alps:
            return "N(#mu) from ALP";
        case pt_mu:
            return "p_{T}(#mu) [GeV]";
        case pt_mu_lead:
            return "p_{T}(#mu)_{lead.} [GeV]";
        case pt_mu_sublead:
            return "p_{T}(#mu)_{sublead.} [GeV]";
        case eta_mu:
            return "#eta(#mu)";
        case eta_mu_lead:
            return "#eta(#mu)_{lead.}";
        case eta_mu_sublead:
            return "#eta(#mu)_{sublead.}";
        case phi_mu:
            return "#varphi(#mu)";
        case phi_mu_lead:
            return "#varphi(#mu)_{lead.}";
        case phi_mu_sublead:
            return "#varphi(#mu)_{sublead.}";
        case m_mumu:
            return "m(#mu#mu) [GeV]";
        case dR_mumu:
            return "#DeltaR(#mu#mu)";
        case aco_mumu:
            return "A_{#phi} = #pi - #Delta#varphi(#mu#mu)";
        case costh_mumu:
            return "cos #theta*(#mu#mu)";
        case N_mu_other:
            return "N(#mu) other";
        case N_tau_cascades:
            return "N(#tau) from #Delta^{#pm#pm}_{61}";
        case N_tau_from_n:
            return "N(#tau) from N";
        case N_tau_from_n3:
            return "N(#tau) from ^{}N_{3}";
        case N_tau_alps:
            return "N(#tau) from ALP";
        case pt_tau:
            return "p_{T}(#tau) [GeV]";
        case pt_tau_lead:
            return "p_{T}(#tau)_{lead.} [GeV]";
        case pt_tau_sublead:
            return "p_{T}(#tau)_{sublead.} [GeV]";
        case eta_tau:
            return "#eta(#tau)";
        case eta_tau_lead:
            return "#eta(#tau)_{lead.}";
        case eta_tau_sublead:
            return "#eta(#tau)_{sublead.}";
        case phi_tau:
            return "#varphi(#tau)";
        case phi_tau_lead:
            return "#varphi(#tau)_{lead.}";
        case phi_tau_sublead:
            return "#varphi(#tau)_{sublead.}";
        case dR_tautau:
            return "#DeltaR(#tau#tau)";
        case N_tau_other:
            return "N(#tau) other";
        case N_lep_cascades:
            return "N(l) from #Delta^{#pm#pm}_{61}";
        case N_lep_from_n:
            return "N(l) from N";
        case pt_lep:
            return "p_{T}(l) [GeV]";
        case pt_lep_lead:
            return "p_{T}(l)_{lead.} [GeV]";
        case pt_lep_sublead:
            return "p_{T}(l)_{sublead.} [GeV]";
        case eta_lep:
            return "#eta(l)";
        case eta_lep_lead:
            return "#eta(l)_{lead.}";
        case eta_lep_sublead:
            return "#eta(l)_{sublead.}";
        case phi_lep:
            return "#varphi(l)";
        case phi_lep_lead:
            return "#varphi(l)_{lead.}";
        case phi_lep_sublead:
            return "#varphi(l)_{sublead.}";
        case dR_leplep:
            return "#DeltaR(ll)";
        case pTratio_lep:
            return "p_{T}(l)_{sublead.} / p_{T}(l)_{lead.} ratio";
        case N_lep_other:
            return "N(l) other";
        case pdgid_lep:
            return "PDGID (l)";
        case N_q_cascades:
            return "N(q) from #Delta^{#pm#pm}_{61}";
        case N_q_hns:
            return "N(q) from N";
        case pt_q:
            return "p_{T}(q) [GeV]";
        case eta_q:
            return "#eta(q)";
        case phi_q:
            return "#varphi(q)";
        case pdgid_q:
            return "PDGID q";
        case N_q_other:
            return "N(q) other";
        case N_jet:
            return "N(j)";
        case pt_jet:
            return "p_{T}(j) [GeV]";
        case pTratio_jet:
            return "p_{T}(j_{#geq 2}) / p_{T}(j_{1}) ratio";
        case pt_jet_lead:
            return "p_{T}(j)_{lead.} [GeV]";
        case pt_jet_sublead:
            return "p_{T}(j)_{sublead.} [GeV]";
        case pt_jet_3:
            return "p_{T}(j_{3}) [GeV]";
        case pt_jet_4:
            return "p_{T}(j_{4}) [GeV]";
        case eta_jet:
            return "#eta(j)";
        case phi_jet:
            return "#varphi(j)";
        case met:
            return "MET [GeV]";
        default:
            return histName;
        }
    }
}
