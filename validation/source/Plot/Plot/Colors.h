#ifndef TN_ENUMS_COLORS_
#define TN_ENUMS_COLORS_

#include <TAttMarker.h>
#include <TColor.h>

namespace BaseColor
{
enum Type : uint16_t {
    White = 0,
    Black = 1,
    Gray = 921,
    Blue = 597,
    Red = 629,
    Green = 418,
    Magenta = 613,
    Cyan = 433,
    Yellow = 397,
    Orange = 797
};
} // namespace BaseColor

namespace Color
{
constexpr uint16_t OFFSET_DARK = 100;
constexpr uint16_t OFFSET_BRIGHT = 150;

enum Type : uint16_t {
    Green = 1179,
    Yellow,
    Purple,
    Red,
    Blue,
    Orange,
    Pink,
    Gray,
    GreenDark = Green + OFFSET_DARK,
    GreenBright = Green + OFFSET_BRIGHT,
    YellowDark = Yellow + OFFSET_DARK,
    YellowBright = Yellow + OFFSET_BRIGHT,
    PurpleDark = Purple + OFFSET_DARK,
    PurpleBright = Purple + OFFSET_BRIGHT,
    RedDark = Red + OFFSET_DARK,
    RedBright = Red + OFFSET_BRIGHT,
    BlueDark = Blue + OFFSET_DARK,
    BlueBright = Blue + OFFSET_BRIGHT,
    OrangeDark = Orange + OFFSET_DARK,
    OrangeBright = Orange + OFFSET_BRIGHT,
    PinkDark = Pink + OFFSET_DARK,
    PinkBright = Pink + OFFSET_BRIGHT,
    GrayDark = Gray + OFFSET_DARK,
    GrayBright = Gray + OFFSET_BRIGHT
};

void setup();
} // namespace Color

const std::vector<int16_t> LINE_COLORS = {BaseColor::Blue, BaseColor::Red, BaseColor::Green, BaseColor::Magenta, BaseColor::Cyan, BaseColor::Yellow, BaseColor::Orange, Color::Purple, Color::Pink, Color::YellowDark, Color::GrayDark};
const std::vector<int16_t> LINE_MARKERS = {kFullCircle, kFullSquare, kFullDiamond, kFullCross};

#endif // TN_ENUMS_COLORS_
