#ifndef PlotUtils_
#define PlotUtils_

#include <regex>
#include <iostream>

#include <TH1.h>
#include <TPad.h>
#include <TROOT.h>
#include <TAxis.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <THStack.h>
#include <TLegendEntry.h>

#include "Variables.h"

//! \brief PlotUtils namespace
namespace PlotUtils
{
    //! \brief Extract a substring from an input string
    std::string extractSubstring(const std::string &input,
                                 char delimiter,
                                 int occurrence);

    //! \brief Find optimal grid for a given number of pads
    std::pair<int, int> findOptimalGrid(int n);

    //! \brief Coordinates to put text to pad.
    double XtoPad(double x);
    double YtoPad(double x);

    //! \brief Create the ATLAS label for the plot
    void setupATLASLabel(float x,
                         float y,
                         float fontsize);

    //! \brief Legend costumisation
    void setupLegend(TLegend *legend);

    //! \brief Object to store decision for log axes
    struct logVariable
    {
        std::string varName;
        bool logx;
        bool logy;

        logVariable(const std::string &h, bool x, bool y) : varName(h), logx(x), logy(y) {}
    };

    //! \brief Setup log axes
    void setupLogAxis(const std::string &histName,
                      const std::vector<logVariable> &logVariables,
                      TCanvas *canvas,
                      TH1 *h);

    //! \brief Print occupancy percentages on a 2D plot
    void printOccupancy(TH1 *hist);

    //! \brief Setup 2D plots
    void setup2Dplots(THStack *outStack,
                      TCanvas *canvas,
                      TLegend *legend);

    //! \brief Create log scale array
    std::vector<double> logScale(uint16_t n,
                                 double start,
                                 double end,
                                 bool rounded = true);

    //! \brief Move overflow to last bin
    void processOverflow(TH1 *hist,
                         bool disableOverflow = false);

    //! \brief Move underflow to first bin
    void processUnderflow(TH1 *hist,
                          bool disableOverflow = false);

    //! \brief Prepare pads on canvas. Adapted from https://root.cern/doc/master/canvas2_8C.html.
    void canvasPartition(TCanvas *canvas, const int nCols, const int nRows,
                         float lMargin, float bMargin,
                         float rMargin, float tMargin);
}

#endif // PlotUtils_
