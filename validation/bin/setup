#!/bin/bash

# Default values
SCRIPT_PATH=$(dirname "$(realpath "$0")")
VALIDATION_PACKAGE_PATH=$(dirname "$SCRIPT_PATH")

# Prepare logging
# shellcheck source=/dev/null
source "${SCRIPT_PATH}/../../utils/logging.sh"

export PYTHONPATH="$VALIDATION_PACKAGE_PATH:$PYTHONPATH"
log_message "setup" "Validation package path set to: $VALIDATION_PACKAGE_PATH and exported to PYTHONPATH."
export ANALYSIS_BUILD_DIR="${VALIDATION_PACKAGE_PATH}/build"
log_message "setup" "ANALYSIS_BUILD_DIR      set to: $ANALYSIS_BUILD_DIR."
export ANALYSIS_RUN_DIR="${VALIDATION_PACKAGE_PATH}/run"
log_message "setup" "ANALYSIS_RUN_DIR        set to: $ANALYSIS_RUN_DIR."

# Check if ANALYSIS_BUILD_DIR already exists
if [ ! -d "$ANALYSIS_BUILD_DIR" ]; then
    log_message "setup" "Creating "'"build"'" directory in $(dirname "$ANALYSIS_BUILD_DIR")."
    mkdir -p "$ANALYSIS_BUILD_DIR"
fi

# Check if ANALYSIS_RUN_DIR already exists
if [ ! -d "$ANALYSIS_RUN_DIR" ]; then
    log_message "setup" "Creating "'"run"'" directory in $(dirname "$ANALYSIS_RUN_DIR")."
    mkdir -p "$ANALYSIS_RUN_DIR"
fi

pushd "$ANALYSIS_BUILD_DIR" > /dev/null || exit
# shellcheck source=/dev/null
log_message "setup" "Setting up AnalysisBase,25.2.44 environment..."
source "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/AtlasSetup/V03-01-09/AtlasSetup/scripts/asetup.sh" AnalysisBase,25.2.44

log_message "setup" "Setting up XRootD..."
lsetup XRootD

if [ ! -d "CMakeFiles" ]; then
    log_message "setup" "First setup detected. Be sure to run "'"source bin/compile"'" next!"
    log_message "setup" "From then on, you will be able to use "'"setup"'", "'"compile"'", and "'"run"'" commands from anywhere to manage the validation package and do the analysis."
else
    # shellcheck source=/dev/null
    source x86_64-*/setup.sh
fi

popd > /dev/null || exit
