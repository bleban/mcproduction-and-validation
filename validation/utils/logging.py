#!/usr/bin/env python

import logging
from typing import Dict


class CustomFormatter(logging.Formatter):
    """Custom logging formatter with color support for different log levels."""

    GREY: str = '\033[38m'
    RED: str = '\033[31m'
    GREEN: str = '\033[32m'
    BLUE: str = '\033[94m'
    ENDCOLOR: str = '\033[0m'

    BASE_FORMAT: str = f"{BLUE}[%(asctime)s]{ENDCOLOR} {GREEN}%(name)s{ENDCOLOR} %(levelname_colored)s%(levelname)s{ENDCOLOR}: %(message)s"

    FORMATS: Dict[int, str] = {
        logging.DEBUG: BASE_FORMAT.replace('%(levelname_colored)s', ''),
        logging.INFO: BASE_FORMAT.replace('%(levelname_colored)s', ''),
        logging.WARNING: BASE_FORMAT.replace('%(levelname_colored)s', RED),
        logging.ERROR: BASE_FORMAT.replace('%(levelname_colored)s', RED),
        logging.CRITICAL: BASE_FORMAT.replace('%(levelname_colored)s', RED),
    }

    def format(self, record: logging.LogRecord) -> str:
        log_fmt: str = self.FORMATS.get(record.levelno, self.BASE_FORMAT)
        formatter = logging.Formatter(log_fmt, datefmt='%d.%m.%Y %H:%M:%S')
        return formatter.format(record)


def setup_logging(name: str, level: str) -> logging.Logger:
    """
    Sets up a logger with a custom formatter.

    Args:
        name (str): Name of the logger.
        level (str): Logging level (e.g., "debug", "info", "warning", "error", "critical").

    Returns:
        logging.Logger: Configured logger instance.
    """

    LOG_LEVELS: Dict[str, int] = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL,
    }

    log_level = LOG_LEVELS.get(level.lower())

    if log_level is None:
        raise ValueError(f"Invalid log level: {level}. Please, choose from {list(LOG_LEVELS.keys())}.")

    logger: logging.Logger = logging.getLogger(name)
    logger.setLevel(log_level)

    if not logger.handlers:
        ch: logging.StreamHandler = logging.StreamHandler()
        ch.setLevel(log_level)
        ch.setFormatter(CustomFormatter())
        logger.addHandler(ch)

    return logger
