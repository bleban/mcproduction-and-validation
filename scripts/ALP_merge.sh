#!/bin/bash

# Prepare logging
source "../utils/logging.sh"

# Define the alphabet characters
ALPHABET=({A..Z})

# Define the values for cbtil and caphi
CBTIL_VALUES=(10 1 1e-2 1e-5)
CAPHI_VALUES=(10 1 1e-2 1e-5)

# Calculate the total number of files
TOTAL_FILES=$((${#CBTIL_VALUES[@]} * ${#CAPHI_VALUES[@]}))

# Slice the alphabet array to the required number of characters
ALPHABET_SLICE=("${ALPHABET[@]:0:$TOTAL_FILES}")

# Initialize the command string
COMMAND="pdftk"

for CBTIL in "${CBTIL_VALUES[@]}"; do
    for CAPHI in "${CAPHI_VALUES[@]}"; do
        # Pop the first character from the alphabet slice
        CHAR="${ALPHABET_SLICE[0]}"
        ALPHABET_SLICE=("${ALPHABET_SLICE[@]:1}")
        # Append the file and its corresponding alphabet character to the command
        COMMAND+=" $CHAR=../validation/run/ALPS/validation_histograms_cbtil_${CBTIL}_caphi_${CAPHI}_TRUTH1.pdf"
    done
done

# Append the shuffle and output options to the command
COMMAND+=" shuffle"
# Slice the alphabet array again to match the number of files
ALPHABET_SLICE=("${ALPHABET[@]:0:$TOTAL_FILES}")
for CHAR in "${ALPHABET_SLICE[@]}"; do
    COMMAND+=" $CHAR"
done
COMMAND+=" output merged.pdf"

# Print out and execute the command
log_message "PDFMerge" "Executing COMMAND:"
echo "$COMMAND"
eval "$COMMAND"

pdfjam merged.pdf -o ALPS_4by4.pdf --nup 4x4 --papersize '{2268px,1628px}'
rm merged.pdf
