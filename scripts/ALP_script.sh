#!/bin/bash

help() {
    echo "This script runs a whole MC Production (Generation, Derivation and Validation) for the ALPS on the desired Cbtil and Caphi grid"
    echo "Usage: run.sh [ -h | --help      ] Print out this message.
              [ -c | --container-path  ] Path to the ALMA9 container.
              [ -p | --plot-only       ] Only create validation plots from precalculated distributions. Can not be used for the first time.
              [ -v | --validation-only ] Only do validation from precalculated DAODs. Can not be used for the first time."
    exit 0
}

# Prepare logging
source "../utils/logging.sh"

# Default values
PLOT_ONLY=false
VALIDATION_ONLY=false
CONTAINER_PATH="/data0/bleban/singularity/alma9-dev_20231019.sif"
UNSET_PYTHIA="$(env | grep "PYTHIA8" | grep -E -o "^[^=]+" | tr '\n' ' ')"

# Command parsing
getopt --test >/dev/null
if [[ $? -ne 4 ]]; then
    log_message "ALP_GRID" "${RED}$(getopt --test) failed in this environment.${ENDCOLOR}"
    exit 1
fi

opt_short=h,c:,p,v
opt_long=help,container-path:,plot-only,validation-only
OPTS=$(getopt --options $opt_short --longoptions $opt_long -- "$@")

eval set -- "$OPTS"

while true; do
    case "$1" in
    -h | --help)
        help
        ;;
    -c | --container-path)
        printf "${BLUE}ARGPARSER${ENDCOLOR}: Using container from ${BLUE}%s${ENDCOLOR}." "$2"
        CONTAINER_PATH="$2"
        shift 2
        ;;
    -p | --plot-only)
        echo -e "${BLUE}ARGPARSER${ENDCOLOR}: Only running in plot mode."
        PLOT_ONLY=true
        shift 1
        ;;
    -v | --validation-only)
        echo -e "${BLUE}ARGPARSER${ENDCOLOR}: Only running the validation."
        VALIDATION_ONLY=true
        shift 1
        ;;
    --)
        shift
        break
        ;;
    *)
        printf "${RED}ARGPARSER${ENDCOLOR}: Unexpected option: %s.\n" "$1"
        ;;
    esac
done


pushd "../" >/dev/null || exit
for CBTIL in 10 1 1e-2 1e-5; do
    for CAPHI in 10 1 1e-2 1e-5; do

        WORKDIR="cbtil_${CBTIL}_caphi_${CAPHI}"
        log_message "ALP_GRID" "Generating CBTIL: ${CBTIL}, CAPHI: ${CAPHI}"

        # Prepare joboptions for the current setup
        ./prepareJOs.sh -t alps -m "10 20 30 50" -s $WORKDIR

        if [[ "$PLOT_ONLY" == true ]]; then
            pushd "validation/run" >/dev/null || exit
            # shellcheck source=/dev/null
            source ../build/x86_64*/setup.sh
            ./run.sh -p
            popd >/dev/null || exit
        else
            if [[ "$VALIDATION_ONLY" == true ]]; then
                # MC Validation
                pushd "validation" >/dev/null || exit
                apptainer exec "$CONTAINER_PATH" /bin/zsh -c \
                    "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh; \
                    source ./setup.sh; \
                    source ./build/x86_64*/setup.sh; \
                    cd run; \
                    ./run.sh"
                popd >/dev/null || exit
            else
                # Change the cbtil and caphi values
                for JO in "joboptions/ALPS/$WORKDIR"/*/*.py; do
                    sed -i "48 s/chff/edff/g" "$JO"
                    sed -i "84 s/1.000000e+00/$CBTIL/g" "$JO"
                    sed -i "85 s/1.000000e+00/$CAPHI/g" "$JO"
                done

                # MC Generation
                apptainer exec "$CONTAINER_PATH" /bin/zsh -c \
                    "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh; \
                    unset $UNSET_PYTHIA; \
                    asetup AthGeneration,master,latest; \
                    rm .asetup.save; \
                    ./generate.sh"

                # MC Derivation
                apptainer exec "$CONTAINER_PATH" /bin/zsh -c \
                    "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh; \
                    asetup Athena,25.0.0; \
                    rm .asetup.save; \
                    ./derivation.sh"

                # MC Validation
                pushd "validation" >/dev/null || exit
                apptainer exec "$CONTAINER_PATH" /bin/zsh -c \
                    "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh; \
                    source ./setup.sh; \
                    source ./compile.sh; \
                    source ./build/x86_64*/setup.sh; \
                    cd run; \
                    ./run.sh"
                popd >/dev/null || exit
            fi
        fi

    done
done
popd >/dev/null || exit

if [ ! -f pdfmanager.sif ]; then
    docker build -t bleban/pdfmanager:latest --compress --squash .
    apptainer build -F pdfmanager.sif docker-daemon://bleban/pdfmanager:latest
fi
apptainer run pdfmanager.sif ./ALP_merge.sh
