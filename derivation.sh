#!/bin/bash

help() {
    SHIFT="\t\t\t"
    echo "This script runs the reconstraction from the derivations using the Athena's Reco_tf.py script."
    echo -e "Usage: derivation.sh\t[ -h | --help        ] Print out this message."
    echo -e "${SHIFT}[ -j | --num-jobs    ] Number of jobs that you want to submit in parallel. Default: 10."
    echo -e "${SHIFT}[ -i | --input-path  ] Input path where you stored EVNTs. It is read from the sourced config.sh by default."
    echo -e "${SHIFT}[ -f | --formats     ] Output derivation formats. Default: TRUTH1,TRUTH3."
    exit 0
}

# Prepare logging
source "utils/logging.sh"

# Default values
SHIFT="\t\t"
NUM_JOBS=10
FORMATS=TRUTH1,TRUTH3

# Command parsing
getopt --test >/dev/null
if [[ $? -ne 4 ]]; then
    log_message "ARGPARSER" "${RED}$(getopt --test) failed in this environment.${ENDCOLOR}"
    exit 1
fi

opt_short=h,j:,i:,f:
opt_long=help,num-jobs:,input-path:,formats:
OPTS=$(getopt --options $opt_short --longoptions $opt_long -- "$@")

eval set -- "$OPTS"

while true; do
    case "$1" in
    -h | --help)
        help
        ;;
    -j | --num-jobs)
        log_message "ARGPARSER" "Submittig $2 jobs in parallel."
        NUM_JOBS="$2"
        shift 2
        ;;
    -i | --input-path)
        log_message "ARGPARSER" "Inputs will be read from: $2/EVNTs."
        EVNT_PATH="$2/EVNTs"
        shift 2
        ;;
    -f | --formats)
        FORMATS="$2"
        # Validate the FORMATS variable
        if [[ "$FORMATS" != "TRUTH1" && "$FORMATS" != "TRUTH3" && "$FORMATS" != "TRUTH1,TRUTH3" ]]; then
            log_message "ARGPARSER" "${RED}Invalid format specified: $FORMATS. Choose between "'"TRUTH1"'", "'"TRUTH3"'" or "'"TRUTH1,TRUTH3"'". Exiting...${ENDCOLOR}"
            exit 2
        fi
        log_message "ARGPARSER" "Will output $2 derivation format(s)."
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *)
        log_message "ARGPARSER" "Unexpected option: ${RED}${1}${ENDCOLOR}."
        ;;
    esac
done

# Only do NUM_JOBS in parallel
maxjobs() {
    while [[ $(jobs -r | wc -l) -gt "$NUM_JOBS" ]]; do
        sleep 10
    done
}

# Check that the input path is set correctly.
# shellcheck source=config.sh
source config.sh
if [[ ! -v EVNT_PATH || ! -v DAOD_PATH ]]; then
    log_message "MCDerivation" "${RED}"'"EVNT_PATH"'" or "'"DAOD_PATH"'" path variable is not set. Check your "'"source config.sh"'" first. It should be produced by running the prepareJOs.sh script. Exiting...${ENDCOLOR}"
    exit 2
fi

# Check if the output already exists and clear it
if [[ -d "$DAOD_PATH" ]]; then
    while true; do
        log_message "MCDerivation" "The OUTPUT destination ${BLUE}DAOD_PATH=${DAOD_PATH}${ENDCOLOR} already exists. If you proceed, it will be overwritten. Are you sure you want to proceed? [Y/N]"
        read -r YESNO
        case $YESNO in
        [Yy]*)
            if [[ "$DAOD_PATH" == *"GRIDPACKS"* ]]; then
                rm -r "${DAOD_PATH:?}"
            else
                find "$DAOD_PATH" -mindepth 1 -maxdepth 1 -type d ! -name "GRIDPACKS" -exec rm -r {} +
            fi
            log_message "MCDerivation" "Finished deleting files..."
            break
            ;;
        [Nn]*)
            log_message "MCDerivation" "Exiting..."
            exit
            ;;
        *)
            log_message "MCDerivation" "Please answer Y or N."
            ;;
        esac
    done
fi

# Loop through the files and run the derivation
for EVNT in "$EVNT_PATH"/*/EVNT.*.*.root; do
    maxjobs

    # Extract mass from the file name
    FILENAME=$(basename "$EVNT")
    DSID=$(basename "$EVNT" | cut -f2 -d.)

    mkdir -p "$DAOD_PATH/$DSID"

    # Go into the OUTPUT directory to save the logs there
    pushd "$DAOD_PATH/$DSID" >/dev/null || exit

    # Logging
    if [[ "$TYPE" == "CASCADES" || "$TYPE" == "ALPS" ]]; then
        MASS=$(basename "$EVNT" | cut -f3 -d. | sed -n 's/[^0-9]\+\([0-9]\+\).*/\1/p')
        log_message "MCDerivation" "Running for mass ${RED}${MASS}${ENDCOLOR} GeV with DSID ${RED}${DSID}${ENDCOLOR}."
        MASS="m$MASS"
    elif [[ "$TYPE" == "2HNS" || "$TYPE" == "4HNS" ]]; then
        MN=$(basename "$EVNT" | cut -f3 -d. | cut -f1 -d_ | sed -n 's/[^0-9]\+\([0-9]\+\).*/\1/p')
        MDD=$(basename "$EVNT" | cut -f3 -d. | cut -f2 -d_ | sed -n 's/[^0-9]\+\([0-9]\+\).*/\1/p')
        log_message "MCDerivation" "Running for masses ${RED}${MN} : ${MDD}${ENDCOLOR} GeV (mN:mDD) with DSID ${RED}${DSID}${ENDCOLOR}."
        MASS="N${MN}_DD${MDD}"
    fi

    log_message "MCDerivation" "Executing:"
    echo ""
    echo -e "${SHIFT}Derivation_tf.py \\"
    echo -e "${SHIFT}    --CA True \\"
    echo -e "${SHIFT}    --inputEVNTFile=$EVNT_PATH/$DSID/$FILENAME \\"
    echo -e "${SHIFT}    --outputDAODFile=$DSID.$MASS.pool.root \\"
    echo -e "${SHIFT}    --formats=$FORMATS \\"
    echo ""

    Derivation_tf.py \
        --CA True \
        --inputEVNTFile="$EVNT_PATH/$DSID/$FILENAME" \
        --outputDAODFile="$DSID.$MASS.pool.root" \
        --formats="$FORMATS" \
        &
    popd >/dev/null || exit

done
wait

echo
log_message "MCDerivation" "Script execution completed successfully."
