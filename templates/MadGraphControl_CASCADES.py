# Settings
saveProcDir = False
gridpack_mode = False

# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MCJobOptionUtils.JOsupport import get_physics_short

Mdp = float(get_physics_short().split('_')[-1].replace('M', ''))

# Some includes that are necessary to interface MadGraph with Pythia
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_MadGraph.py')

# Number of events to produce
assert (runArgs.maxEvents, runArgs.ecmEnergy, runArgs.randomSeed)
safety = 1.3  # safety factor to account for filter efficiency
nevents = runArgs.maxEvents * safety if runArgs.maxEvents > 0 else evgenConfig.nEventsPerJob * safety

# Import the Type-II seesaw UFO model and define the process
if not is_gen_from_gridpack():
    process = '''
    import model TypeII_NLO_v_1_4_UFO
    define dpm = d+ d-
    define dppmm = d++ d--
    generate p p > d+ d- [QCD]
    add process p p > d0 dpm [QCD]
    add process p p > d0 chi [QCD]
    add process p p > dpm chi [QCD]
    add process p p > dppmm dpm [QCD]
    add process p p > d++ d-- [QCD]
    output -f
    '''
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

# Set some values in the run_card
settings = {
    'ickkw': 0,
    'nevents': nevents
}

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

# Set some values in the param card
# BSM particle masses d++ mass < d+ mass < d0 mass
masses = {
    'MDP': Mdp,
    'MDPP': Mdp - 50.
}

# BSM particle width set to auto
decays = {
    'wdpp': 'auto',
    'wdp': 'auto',
    'wd0': 'auto',
    'wchi': 'auto'
}

# Modify the vev of the heavy triplet
vevD = {'1': '1.000000e-06 # vevD'}

# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir, params={
                  'MASS': masses, 'DECAY': decays, 'vevdelta': vevD})

# Do the event generation
generate(process_dir=process_dir, grid_pack=gridpack_mode, runArgs=runArgs)

# These details are important information about the JOs
evgenConfig.description = f'Type-II seesaw model in the cascades regime. mdp = {Mdp} GeV'
evgenConfig.contact = ['Blaž Leban <blaz.leban@cern.ch>']
evgenConfig.keywords += ['BSM', 'exotic', 'sameSign', 'multilepton', 'chargedHiggs', 'seeSaw', 'BSMHiggs']
evgenConfig.generators = ['MadGraph']
evgenConfig.nEventsPerJob = 10000

arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=saveProcDir)

# # Apply filters to get the decays we want
# # This is necessary for xAOD filters
# # IMPORTANT: Make sure to increase the safety factor!
# include ('GeneratorFilters/CreatexAODSlimContainers.py')

# # Import the filters and add them to the filter sequence
# from GeneratorFilters.GeneratorFiltersConf import xAODDecaysFinalStateFilter
# filtSeq += xAODDecaysFinalStateFilter('eeFilter')
# filtSeq += xAODDecaysFinalStateFilter('mmFilter')
# filtSeq += xAODDecaysFinalStateFilter('emFilter')
# filtSeq += xAODDecaysFinalStateFilter('tauFilter')

# # Set allowed parent PDG IDs
# filtSeq.eeFilter.PDGAllowedParents =  [61, -61]
# filtSeq.mmFilter.PDGAllowedParents =  [61, -61]
# filtSeq.emFilter.PDGAllowedParents =  [61, -61]
# filtSeq.tauFilter.PDGAllowedParents = [61, -61]

# # Set minimum number of allowed leptons
# filtSeq.eeFilter.NElectrons = 2
# filtSeq.mmFilter.NMuons = 2
# filtSeq.emFilter.NElectrons = 1
# filtSeq.emFilter.NMuons = 1
# filtSeq.tauFilter.NTaus = 0

# # Set the filter message levels to DEBUG so we can see what is happening
# filtSeq.eeFilter.OutputLevel =  DEBUG
# filtSeq.mmFilter.OutputLevel =  DEBUG
# filtSeq.emFilter.OutputLevel =  DEBUG
# filtSeq.tauFilter.OutputLevel = DEBUG

# # Implement the logic for passing the set of filters
# filtSeq.Expression = 'eeFilter or mmFilter or emFilter and tauFilter'
