# Settings
saveProcDir = False
gridpack_mode = False

Max = 20.

# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MCJobOptionUtils.JOsupport import get_physics_short

Max = float(get_physics_short().split('_')[-1].replace('M', ''))

# Some includes that are necessary to interface MadGraph with Pythia
include('Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_EE_Common.py')
include('Pythia8_i/Pythia8_LHEF.py')

# remove TestHepMC because this configuration is an 'unconventional' usage of Madgraph
if hasattr(testSeq, 'TestHepMC'): testSeq.remove(TestHepMC())

# Number of events to produce
safety = 1.1
nevents = runArgs.maxEvents * safety if runArgs.maxEvents > 0 else evgenConfig.nEventsPerJob * safety

# Import model and define the process
if not is_gen_from_gridpack():
    process = '''
    import model ALP_linear_UFO_WIDTH
    generate a a > ax > mu+ mu-
    output -f
    '''
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

# Set some values in the run_card
energy = str(runArgs.ecmEnergy / 2 * 208)  # in GeV
settings = {
    'lpp1': '2',
    'lpp2': '2',
    'ebeam1': energy,  # beam 1 energy
    'ebeam2': energy,  # beam 2 energy
    'pdlabel': 'edff',
    'nb_proton1': '82',  # 208 Pb 82
    'nb_proton2': '82',  # 208 Pb 82
    'nb_neutron1': '126',  # 208 Pb 82
    'nb_neutron2': '126',  # 208 Pb 82
    'ptl': '3.5',  # min pT of final state leptons
    'ptlmax': '-1',  # max pT of final state leptons (-1 = no cut)
    'etal': '2.7',  # max abs(eta) of final state leptons
    'etalmin': '-2.7',  # main rap for the charged leptons
    'mmll': '7.0',  # min invariant mass between final state leptons
    'mmllmax': '-1',  # max invariant mass between final state leptons
    'use_syst': 'False',
    'systematics_program': 'None',
    'time_of_flight': 1e-25,
    'nevents': nevents
}

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)


# Set some values in the param_card
#  Alp masses
masses = {
    '9000005': Max
}

# Decay Width
decays = {
    '9000005': 'auto'
}

# ALP parameters
couplings = {
    'fa': 1.000000e+03,
    'cgtil': 0.000000e+00,
    'cwtil': 0.000000e+00,
    'cbtil': 1.000000e+00,
    'caphi': 1.000000e+00
}

# Yukawa couplings
yukawas = {
    'ymdo': 0.000000e+00,
    'ymup': 0.000000e+00,
    'yms': 0.000000e+00,
    'ymc': 0.000000e+00,
    'ymb': 0.000000e+00,
    'ymt': 0.000000e+00,
    'yme': 0.000000e+00,
    'ymm': 1.056600e-01,
    'ymtau': 0.000000e+00
}

# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir, params={
                  'MASS': masses, 'DECAY': decays, 'ALPPARS': couplings,
                  'YUKAWA': yukawas})

# Do the event generation
generate(process_dir=process_dir, grid_pack=gridpack_mode, runArgs=runArgs)

# These details are important information about the JOs
evgenConfig.description = 'Axion production in yy -> mu+ mu- process'
evgenConfig.contact = ['Karolina Domijan <karolina.domijan@cern.ch>']
evgenConfig.keywords += ['2photon', '2muon']
evgenConfig.generators += ['MadGraph']
evgenConfig.nEventsPerJob = 10000

arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=saveProcDir)

testSeq.TestHepMC.MaxTransVtxDisp = 1e+08  # in mm
testSeq.TestHepMC.MaxVtxDisp = 1e+08  # in mm
testSeq.TestHepMC.MaxNonG4Energy = 1e+08  # in MeV
