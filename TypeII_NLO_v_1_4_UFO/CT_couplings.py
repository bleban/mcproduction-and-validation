# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 13.1.0 for Mac OS X ARM (64-bit) (June 16, 2022)
# Date: Tue 24 Jan 2023 17:25:57


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_410_1 = Coupling(name = 'R2GC_410_1',
                      value = '-0.0625*(complex(0,1)*G**2)/cmath.pi**2',
                      order = {'QCD':2})

R2GC_411_2 = Coupling(name = 'R2GC_411_2',
                      value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_412_3 = Coupling(name = 'R2GC_412_3',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_414_4 = Coupling(name = 'R2GC_414_4',
                      value = '-0.05555555555555555*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_419_5 = Coupling(name = 'R2GC_419_5',
                      value = '-0.125*(complex(0,1)*G**2*MB**2)/cmath.pi**2',
                      order = {'QCD':2})

R2GC_419_6 = Coupling(name = 'R2GC_419_6',
                      value = '-0.125*(complex(0,1)*G**2*MT**2)/cmath.pi**2',
                      order = {'QCD':2})

R2GC_420_7 = Coupling(name = 'R2GC_420_7',
                      value = '-0.125*(cxi*complex(0,1)*G**2*MB*yb)/(cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_420_8 = Coupling(name = 'R2GC_420_8',
                      value = '-0.125*(cxi*complex(0,1)*G**2*MT*yt)/(cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_421_9 = Coupling(name = 'R2GC_421_9',
                      value = '(complex(0,1)*G**2*MB*sxi*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_421_10 = Coupling(name = 'R2GC_421_10',
                       value = '(complex(0,1)*G**2*MT*sxi*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_422_11 = Coupling(name = 'R2GC_422_11',
                       value = '-0.0625*(cxi**2*complex(0,1)*G**2*yb**2)/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_422_12 = Coupling(name = 'R2GC_422_12',
                       value = '-0.0625*(cxi**2*complex(0,1)*G**2*yt**2)/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_423_13 = Coupling(name = 'R2GC_423_13',
                       value = '(cxi*complex(0,1)*G**2*sxi*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_423_14 = Coupling(name = 'R2GC_423_14',
                       value = '(cxi*complex(0,1)*G**2*sxi*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_424_15 = Coupling(name = 'R2GC_424_15',
                       value = '-0.0625*(complex(0,1)*G**2*sxi**2*yb**2)/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_424_16 = Coupling(name = 'R2GC_424_16',
                       value = '-0.0625*(complex(0,1)*G**2*sxi**2*yt**2)/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_425_17 = Coupling(name = 'R2GC_425_17',
                       value = '-0.0625*(complex(0,1)*G**2*vev**2*yb**2)/(cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_425_18 = Coupling(name = 'R2GC_425_18',
                       value = '-0.0625*(complex(0,1)*G**2*vev**2*yt**2)/(cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_426_19 = Coupling(name = 'R2GC_426_19',
                       value = '(complex(0,1)*G**2*vev*vevD*yb**2)/(8.*cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_426_20 = Coupling(name = 'R2GC_426_20',
                       value = '(complex(0,1)*G**2*vev*vevD*yt**2)/(8.*cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_427_21 = Coupling(name = 'R2GC_427_21',
                       value = '-0.25*(complex(0,1)*G**2*vevD**2*yb**2)/(cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_427_22 = Coupling(name = 'R2GC_427_22',
                       value = '-0.25*(complex(0,1)*G**2*vevD**2*yt**2)/(cmath.pi**2*(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_428_23 = Coupling(name = 'R2GC_428_23',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_429_24 = Coupling(name = 'R2GC_429_24',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_429_25 = Coupling(name = 'R2GC_429_25',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_430_26 = Coupling(name = 'R2GC_430_26',
                       value = '-0.006944444444444444*(ee*complex(0,1)*G**3)/cmath.pi**2',
                       order = {'QCD':3,'QED':1})

R2GC_430_27 = Coupling(name = 'R2GC_430_27',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_431_28 = Coupling(name = 'R2GC_431_28',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_431_29 = Coupling(name = 'R2GC_431_29',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_432_30 = Coupling(name = 'R2GC_432_30',
                       value = '-0.005208333333333333*(cw*ee*complex(0,1)*G**3)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_432_31 = Coupling(name = 'R2GC_432_31',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_433_32 = Coupling(name = 'R2GC_433_32',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_433_33 = Coupling(name = 'R2GC_433_33',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_434_34 = Coupling(name = 'R2GC_434_34',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_434_35 = Coupling(name = 'R2GC_434_35',
                       value = '-0.003472222222222222*(ee**2*complex(0,1)*G**2)/cmath.pi**2 + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_435_36 = Coupling(name = 'R2GC_435_36',
                       value = '-0.08333333333333333*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_436_37 = Coupling(name = 'R2GC_436_37',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_438_38 = Coupling(name = 'R2GC_438_38',
                       value = '-0.0625*(complex(0,1)*G**2*vev**2*yb**2)/(cmath.pi**2*(vev**2 + 2*vevD**2)) - (complex(0,1)*G**2*vev**2*yt**2)/(16.*cmath.pi**2*(vev**2 + 2*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_439_39 = Coupling(name = 'R2GC_439_39',
                       value = '(complex(0,1)*G**2*vev*vevD*yb**2)/(8.*cmath.pi**2*(vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (complex(0,1)*G**2*vev*vevD*yt**2)/(8.*cmath.pi**2*(vev**2 + 2*vevD**2)*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':2})

R2GC_440_40 = Coupling(name = 'R2GC_440_40',
                       value = '-0.125*(complex(0,1)*G**2*vevD**2*yb**2)/(cmath.pi**2*(vev**2 + 2*vevD**2)) - (complex(0,1)*G**2*vevD**2*yt**2)/(8.*cmath.pi**2*(vev**2 + 2*vevD**2))',
                       order = {'QCD':2,'QED':2})

R2GC_441_41 = Coupling(name = 'R2GC_441_41',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_453_42 = Coupling(name = 'R2GC_453_42',
                       value = '-0.015625*G**3/cmath.pi**2',
                       order = {'QCD':3})

R2GC_454_43 = Coupling(name = 'R2GC_454_43',
                       value = 'G**3/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_455_44 = Coupling(name = 'R2GC_455_44',
                       value = '-0.005208333333333333*G**4/cmath.pi**2',
                       order = {'QCD':4})

R2GC_455_45 = Coupling(name = 'R2GC_455_45',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_456_46 = Coupling(name = 'R2GC_456_46',
                       value = '-0.005208333333333333*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_456_47 = Coupling(name = 'R2GC_456_47',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_457_48 = Coupling(name = 'R2GC_457_48',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_457_49 = Coupling(name = 'R2GC_457_49',
                       value = '-0.015625*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_458_50 = Coupling(name = 'R2GC_458_50',
                       value = '-0.020833333333333332*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_459_51 = Coupling(name = 'R2GC_459_51',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_459_52 = Coupling(name = 'R2GC_459_52',
                       value = '-0.03125*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_460_53 = Coupling(name = 'R2GC_460_53',
                       value = '-0.0625*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_461_54 = Coupling(name = 'R2GC_461_54',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_462_55 = Coupling(name = 'R2GC_462_55',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_463_56 = Coupling(name = 'R2GC_463_56',
                       value = '-0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2',
                       order = {'QCD':3})

R2GC_464_57 = Coupling(name = 'R2GC_464_57',
                       value = '-0.1111111111111111*(ee*complex(0,1)*G**2)/cmath.pi**2',
                       order = {'QCD':2,'QED':1})

R2GC_478_58 = Coupling(name = 'R2GC_478_58',
                       value = '-0.16666666666666666*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_483_59 = Coupling(name = 'R2GC_483_59',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_486_60 = Coupling(name = 'R2GC_486_60',
                       value = '(cxi*complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_487_61 = Coupling(name = 'R2GC_487_61',
                       value = '-0.3333333333333333*(complex(0,1)*G**2*sxi*yb)/(cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_488_62 = Coupling(name = 'R2GC_488_62',
                       value = '-0.3333333333333333*(G**2*vev*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_489_63 = Coupling(name = 'R2GC_489_63',
                       value = '(G**2*vevD*yb*cmath.sqrt(2))/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_491_64 = Coupling(name = 'R2GC_491_64',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_491_65 = Coupling(name = 'R2GC_491_65',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_492_66 = Coupling(name = 'R2GC_492_66',
                       value = '(5*G**3)/(32.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_493_67 = Coupling(name = 'R2GC_493_67',
                       value = '(3*G**3)/(16.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_494_68 = Coupling(name = 'R2GC_494_68',
                       value = '-0.041666666666666664*G**3/cmath.pi**2',
                       order = {'QCD':3})

R2GC_494_69 = Coupling(name = 'R2GC_494_69',
                       value = '(-5*G**3)/(32.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_495_70 = Coupling(name = 'R2GC_495_70',
                       value = '(-11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_496_71 = Coupling(name = 'R2GC_496_71',
                       value = '(-3*G**3)/(16.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_497_72 = Coupling(name = 'R2GC_497_72',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_497_73 = Coupling(name = 'R2GC_497_73',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_498_74 = Coupling(name = 'R2GC_498_74',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_498_75 = Coupling(name = 'R2GC_498_75',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_500_76 = Coupling(name = 'R2GC_500_76',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_501_77 = Coupling(name = 'R2GC_501_77',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_506_78 = Coupling(name = 'R2GC_506_78',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_510_79 = Coupling(name = 'R2GC_510_79',
                       value = '(complex(0,1)*G**2*vev*yb)/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_511_80 = Coupling(name = 'R2GC_511_80',
                       value = '-0.3333333333333333*(complex(0,1)*G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(vev**2/2. + vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_512_81 = Coupling(name = 'R2GC_512_81',
                       value = '(cxi*complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_513_82 = Coupling(name = 'R2GC_513_82',
                       value = '-0.3333333333333333*(complex(0,1)*G**2*sxi*yt)/(cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_514_83 = Coupling(name = 'R2GC_514_83',
                       value = '-0.3333333333333333*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_515_84 = Coupling(name = 'R2GC_515_84',
                       value = '(complex(0,1)*G**2*vevD*yt)/(3.*cmath.pi**2*cmath.sqrt(vev**2/2. + vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_516_85 = Coupling(name = 'R2GC_516_85',
                       value = '(G**2*vev*yt)/(3.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':1})

R2GC_517_86 = Coupling(name = 'R2GC_517_86',
                       value = '-0.3333333333333333*(G**2*vevD*yt*cmath.sqrt(2))/(cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2))',
                       order = {'QCD':2,'QED':1})

UVGC_443_1 = Coupling(name = 'UVGC_443_1',
                      value = {-1:'(-3*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_444_2 = Coupling(name = 'UVGC_444_2',
                      value = {-1:'-0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2'},
                      order = {'QCD':2})

UVGC_445_3 = Coupling(name = 'UVGC_445_3',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_447_4 = Coupling(name = 'UVGC_447_4',
                      value = {-1:'-0.027777777777777776*(ee*complex(0,1)*G**2)/cmath.pi**2'},
                      order = {'QCD':2,'QED':1})

UVGC_452_5 = Coupling(name = 'UVGC_452_5',
                      value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_452_6 = Coupling(name = 'UVGC_452_6',
                      value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_453_7 = Coupling(name = 'UVGC_453_7',
                      value = {-1:'(-11*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_453_8 = Coupling(name = 'UVGC_453_8',
                      value = {-1:'G**3/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_454_9 = Coupling(name = 'UVGC_454_9',
                      value = {-1:'(11*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_454_10 = Coupling(name = 'UVGC_454_10',
                       value = {-1:'G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_455_11 = Coupling(name = 'UVGC_455_11',
                       value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_455_12 = Coupling(name = 'UVGC_455_12',
                       value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_456_13 = Coupling(name = 'UVGC_456_13',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_456_14 = Coupling(name = 'UVGC_456_14',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_458_15 = Coupling(name = 'UVGC_458_15',
                       value = {-1:'-0.0078125*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_458_16 = Coupling(name = 'UVGC_458_16',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_459_17 = Coupling(name = 'UVGC_459_17',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_459_18 = Coupling(name = 'UVGC_459_18',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_460_19 = Coupling(name = 'UVGC_460_19',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_461_20 = Coupling(name = 'UVGC_461_20',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_462_21 = Coupling(name = 'UVGC_462_21',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_463_22 = Coupling(name = 'UVGC_463_22',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_464_23 = Coupling(name = 'UVGC_464_23',
                       value = {-1:'-0.05555555555555555*(ee*complex(0,1)*G**2)/cmath.pi**2'},
                       order = {'QCD':2,'QED':1})

UVGC_465_24 = Coupling(name = 'UVGC_465_24',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_465_25 = Coupling(name = 'UVGC_465_25',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_465_26 = Coupling(name = 'UVGC_465_26',
                       value = {-1:'-0.0078125*(complex(0,1)*G**3)/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_465_27 = Coupling(name = 'UVGC_465_27',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_478_28 = Coupling(name = 'UVGC_478_28',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_478_29 = Coupling(name = 'UVGC_478_29',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_480_30 = Coupling(name = 'UVGC_480_30',
                       value = {-1:'(complex(0,1)*G**2)/(4.*cmath.pi**2)',0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_481_31 = Coupling(name = 'UVGC_481_31',
                       value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',0:'(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_482_32 = Coupling(name = 'UVGC_482_32',
                       value = {-1:'-0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2',0:'-0.3333333333333333*(complex(0,1)*G**3)/cmath.pi**2 + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_483_33 = Coupling(name = 'UVGC_483_33',
                       value = {-1:'(complex(0,1)*G**2*MB)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MB)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_484_34 = Coupling(name = 'UVGC_484_34',
                       value = {-1:'(cw*ee*complex(0,1)*G**2)/(8.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(24.*cw*cmath.pi**2)',0:'(cw*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_485_35 = Coupling(name = 'UVGC_485_35',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2)',0:'-0.1111111111111111*(ee*complex(0,1)*G**2*sw)/(cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_486_36 = Coupling(name = 'UVGC_486_36',
                       value = {-1:'(cxi*complex(0,1)*G**2*yb)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(cxi*complex(0,1)*G**2*yb*cmath.sqrt(2))/(3.*cmath.pi**2) - (cxi*complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_487_37 = Coupling(name = 'UVGC_487_37',
                       value = {-1:'-0.5*(complex(0,1)*G**2*sxi*yb)/(cmath.pi**2*cmath.sqrt(2))',0:'-0.3333333333333333*(complex(0,1)*G**2*sxi*yb*cmath.sqrt(2))/cmath.pi**2 + (complex(0,1)*G**2*sxi*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_488_38 = Coupling(name = 'UVGC_488_38',
                       value = {-1:'-0.5*(G**2*vev*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',0:'-0.3333333333333333*(G**2*vev*yb*cmath.sqrt(2))/(cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2)) + (G**2*vev*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_489_39 = Coupling(name = 'UVGC_489_39',
                       value = {-1:'(G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',0:'(2*G**2*vevD*yb*cmath.sqrt(2))/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2)) - (G**2*vevD*yb*cmath.sqrt(2)*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_490_40 = Coupling(name = 'UVGC_490_40',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**2)/cmath.pi**2',0:'(complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_490_41 = Coupling(name = 'UVGC_490_41',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**2)/cmath.pi**2',0:'(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_491_42 = Coupling(name = 'UVGC_491_42',
                       value = {-1:'G**3/(24.*cmath.pi**2)',0:'-0.08333333333333333*(G**3*reglog(MB/MU_R))/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_491_43 = Coupling(name = 'UVGC_491_43',
                       value = {-1:'-0.020833333333333332*G**3/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_491_44 = Coupling(name = 'UVGC_491_44',
                       value = {-1:'(39*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_491_45 = Coupling(name = 'UVGC_491_45',
                       value = {-1:'G**3/(24.*cmath.pi**2)',0:'-0.08333333333333333*(G**3*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_492_46 = Coupling(name = 'UVGC_492_46',
                       value = {-1:'(31*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_492_47 = Coupling(name = 'UVGC_492_47',
                       value = {-1:'(3*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_493_48 = Coupling(name = 'UVGC_493_48',
                       value = {-1:'(53*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_493_49 = Coupling(name = 'UVGC_493_49',
                       value = {-1:'G**3/(32.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_494_50 = Coupling(name = 'UVGC_494_50',
                       value = {-1:'-0.041666666666666664*G**3/cmath.pi**2',0:'(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_494_51 = Coupling(name = 'UVGC_494_51',
                       value = {-1:'G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_494_52 = Coupling(name = 'UVGC_494_52',
                       value = {-1:'(-31*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_494_53 = Coupling(name = 'UVGC_494_53',
                       value = {-1:'-0.041666666666666664*G**3/cmath.pi**2',0:'(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_495_54 = Coupling(name = 'UVGC_495_54',
                       value = {-1:'(-45*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_495_55 = Coupling(name = 'UVGC_495_55',
                       value = {-1:'-0.015625*G**3/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_496_56 = Coupling(name = 'UVGC_496_56',
                       value = {-1:'(-53*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_496_57 = Coupling(name = 'UVGC_496_57',
                       value = {-1:'-0.0078125*G**3/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_497_58 = Coupling(name = 'UVGC_497_58',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'-0.08333333333333333*(complex(0,1)*G**4*reglog(MB/MU_R))/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_497_59 = Coupling(name = 'UVGC_497_59',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_497_60 = Coupling(name = 'UVGC_497_60',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_497_61 = Coupling(name = 'UVGC_497_61',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'-0.08333333333333333*(complex(0,1)*G**4*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_498_62 = Coupling(name = 'UVGC_498_62',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_498_63 = Coupling(name = 'UVGC_498_63',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_499_64 = Coupling(name = 'UVGC_499_64',
                       value = {0:'-0.08333333333333333*(complex(0,1)*G**4*reglog(MB/MU_R))/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_499_65 = Coupling(name = 'UVGC_499_65',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_499_66 = Coupling(name = 'UVGC_499_66',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_499_67 = Coupling(name = 'UVGC_499_67',
                       value = {0:'-0.08333333333333333*(complex(0,1)*G**4*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_500_68 = Coupling(name = 'UVGC_500_68',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**4)/cmath.pi**2',0:'(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_500_69 = Coupling(name = 'UVGC_500_69',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_500_70 = Coupling(name = 'UVGC_500_70',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_500_71 = Coupling(name = 'UVGC_500_71',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_500_72 = Coupling(name = 'UVGC_500_72',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**4)/cmath.pi**2',0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_501_73 = Coupling(name = 'UVGC_501_73',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_501_74 = Coupling(name = 'UVGC_501_74',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_502_75 = Coupling(name = 'UVGC_502_75',
                       value = {0:'(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_502_76 = Coupling(name = 'UVGC_502_76',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_502_77 = Coupling(name = 'UVGC_502_77',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_502_78 = Coupling(name = 'UVGC_502_78',
                       value = {0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_503_79 = Coupling(name = 'UVGC_503_79',
                       value = {-1:'(complex(0,1)*G**2)/(4.*cmath.pi**2)',0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_504_80 = Coupling(name = 'UVGC_504_80',
                       value = {-1:'-0.1111111111111111*(ee*complex(0,1)*G**2)/cmath.pi**2',0:'(-2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_505_81 = Coupling(name = 'UVGC_505_81',
                       value = {-1:'-0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2',0:'-0.3333333333333333*(complex(0,1)*G**3)/cmath.pi**2 + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_506_82 = Coupling(name = 'UVGC_506_82',
                       value = {-1:'(complex(0,1)*G**2*MT)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_507_83 = Coupling(name = 'UVGC_507_83',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))',0:'-0.16666666666666666*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_507_84 = Coupling(name = 'UVGC_507_84',
                       value = {-1:'-0.08333333333333333*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2))',0:'-0.16666666666666666*(ee*complex(0,1)*G**2)/(cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_508_85 = Coupling(name = 'UVGC_508_85',
                       value = {-1:'-0.125*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(24.*cw*cmath.pi**2)',0:'-0.16666666666666666*(cw*ee*complex(0,1)*G**2)/(cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_509_86 = Coupling(name = 'UVGC_509_86',
                       value = {-1:'(ee*complex(0,1)*G**2*sw)/(6.*cw*cmath.pi**2)',0:'(2*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_510_87 = Coupling(name = 'UVGC_510_87',
                       value = {-1:'(complex(0,1)*G**2*vev*yb)/(12.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',0:'(complex(0,1)*G**2*vev*yb)/(2.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2)) - (3*complex(0,1)*G**2*vev*yb*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_510_88 = Coupling(name = 'UVGC_510_88',
                       value = {-1:'(complex(0,1)*G**2*vev*yb)/(12.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',0:'(complex(0,1)*G**2*vev*yb)/(6.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*G**2*vev*yb*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_510_89 = Coupling(name = 'UVGC_510_89',
                       value = {-1:'(complex(0,1)*G**2*vev*yb)/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_511_90 = Coupling(name = 'UVGC_511_90',
                       value = {-1:'-0.16666666666666666*(complex(0,1)*G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',0:'-((complex(0,1)*G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) + (3*complex(0,1)*G**2*vevD*yb*reglog(MB/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_511_91 = Coupling(name = 'UVGC_511_91',
                       value = {-1:'-0.16666666666666666*(complex(0,1)*G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',0:'-0.3333333333333333*(complex(0,1)*G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) + (complex(0,1)*G**2*vevD*yb*reglog(MT/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_511_92 = Coupling(name = 'UVGC_511_92',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*vevD*yb)/(cmath.pi**2*cmath.sqrt(vev**2/2. + vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_512_93 = Coupling(name = 'UVGC_512_93',
                       value = {-1:'(cxi*complex(0,1)*G**2*yt)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(cxi*complex(0,1)*G**2*yt*cmath.sqrt(2))/(3.*cmath.pi**2) - (cxi*complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_513_94 = Coupling(name = 'UVGC_513_94',
                       value = {-1:'-0.5*(complex(0,1)*G**2*sxi*yt)/(cmath.pi**2*cmath.sqrt(2))',0:'-0.3333333333333333*(complex(0,1)*G**2*sxi*yt*cmath.sqrt(2))/cmath.pi**2 + (complex(0,1)*G**2*sxi*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_514_95 = Coupling(name = 'UVGC_514_95',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',0:'-0.16666666666666666*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2)) + (complex(0,1)*G**2*vev*yt*reglog(MB/MU_R))/(4.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_514_96 = Coupling(name = 'UVGC_514_96',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))',0:'-0.5*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2)) + (3*complex(0,1)*G**2*vev*yt*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_514_97 = Coupling(name = 'UVGC_514_97',
                       value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*vev*yt)/(cmath.pi**2*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_515_98 = Coupling(name = 'UVGC_515_98',
                       value = {-1:'(complex(0,1)*G**2*vevD*yt)/(6.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',0:'(complex(0,1)*G**2*vevD*yt)/(3.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*G**2*vevD*yt*reglog(MB/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_515_99 = Coupling(name = 'UVGC_515_99',
                       value = {-1:'(complex(0,1)*G**2*vevD*yt)/(6.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',0:'(complex(0,1)*G**2*vevD*yt)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (3*complex(0,1)*G**2*vevD*yt*reglog(MT/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))'},
                       order = {'QCD':2,'QED':1})

UVGC_515_100 = Coupling(name = 'UVGC_515_100',
                        value = {-1:'(complex(0,1)*G**2*vevD*yt)/(3.*cmath.pi**2*cmath.sqrt(vev**2/2. + vevD**2))'},
                        order = {'QCD':2,'QED':1})

UVGC_516_101 = Coupling(name = 'UVGC_516_101',
                        value = {-1:'(G**2*vev*yt)/(2.*cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',0:'(G**2*vev*yt*cmath.sqrt(2))/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2)) - (G**2*vev*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))'},
                        order = {'QCD':2,'QED':1})

UVGC_517_102 = Coupling(name = 'UVGC_517_102',
                        value = {-1:'-((G**2*vevD*yt)/(cmath.pi**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)))',0:'(-2*G**2*vevD*yt*cmath.sqrt(2))/(3.*cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2)) + (G**2*vevD*yt*cmath.sqrt(2)*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(vev**2 + 4*vevD**2))'},
                        order = {'QCD':2,'QED':1})

