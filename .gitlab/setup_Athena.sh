#!/bin/bash

help() {
    echo "This script is used in a CI job to set up the Athena/AthGeneration release."
    echo -e "Usage: setup_Athena.sh <flags>\n"
    echo "Available flags:"
    echo -e "\t+ [ -h | --help        ] Print out this message."
    echo -e "\t+ [ -p | --project     ] Project name. Choose from Athena or AthGeneration. Default: Athena."
    echo -e "\t+ [ -r | --release     ] Release number. Default: main,latest."
    echo -e "\t+ [ -l | --enable-echo ] Enable echoing of job transform to stdout. Default: false."
    echo -e "\t+ [ -x | --xrootd      ] Setup XRootD package. Default: false."
    exit 0
}

# Defaults
PROJECT=Athena
RELEASE=main,latest
XROOTD=false
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

# Prepare logging
# shellcheck source=../utils/logging.sh
source "${SCRIPT_DIR}/../utils/logging.sh"

# Command parsing
getopt --test >/dev/null
if [[ $? -ne 4 ]]; then
    log_message "setup_Athena" "setup_Athena" "${RED}$(getopt --test) failed in this environment.${ENDCOLOR}"
    exit 1
fi

opt_short=h,p:,r:,l,x
opt_long=help,project:,release:,enable-echo,xrootd
OPTS=$(getopt --options $opt_short --longoptions $opt_long -- "$@")

eval set -- "$OPTS"

while true; do
    case "$1" in
    -h | --help)
        help
        ;;
    -p | --project)
        printf "${BLUE}ARGPARSER${ENDCOLOR}: Will set up the %s project.\n" "$2"
        PROJECT="$2"
        shift 2
        ;;
    -r | --release)
        printf "${BLUE}ARGPARSER${ENDCOLOR}: Will set up the release: %s.\n" "$2"
        RELEASE="$2"
        shift 2
        ;;
    -l | --enable-echo)
        echo -e "${BLUE}ARGPARSER${ENDCOLOR}: Enabling echoing of job transform to stdout: export TRF_ECHO=1"
        export TRF_ECHO=1
        shift
        ;;
    -x | --xrootd)
        echo -e "${BLUE}ARGPARSER${ENDCOLOR}: Will set up the XRootD package."
        XROOTD=true
        shift
        ;;
    --)
        shift
        break
        ;;
    *)
        printf "${RED}ARGPARSER${ENDCOLOR}: Unexpected option: %s.\n" "$1"
        ;;
    esac
done

log_message "setup_Athena" "Setting up the ATLAS environment..."
export ATLAS_LOCAL_ROOT_BASE='/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase'
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

log_message "setup_Athena" "------------------------------------------------------"
log_message "setup_Athena" "    Setting up the $PROJECT release v$RELEASE    "
log_message "setup_Athena" "------------------------------------------------------"
asetup "$PROJECT","$RELEASE"

log_message "setup_Athena" "Done setting up the $PROJECT release!"

if [[ "$XROOTD" == true ]]; then
    log_message "setup_Athena" "Setting up the XRootD package..."
    lsetup xrootd
fi