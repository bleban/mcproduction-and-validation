#!/bin/bash

help() {
    echo "This script is used in a CI job to move the GRIDPACKs from production folder to the EVNT generation folder."
    echo -e "Usage: move_gridpack.sh <flags>\n"
    echo "Available flags:"
    echo -e "\t+ [ -h | --help ] Print out this message and exit."
    echo -e "\t+ [ -p | --path ] Path to the EVNTs folder. Mandatory flag. Default: None which fails."
    exit 0
}

# Defaults
EVNT_PATH_GEN=""
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

# Prepare logging
# shellcheck source=../utils/logging.sh
source "${SCRIPT_DIR}/../utils/logging.sh"

# Command parsing
getopt --test >/dev/null
if [[ $? -ne 4 ]]; then
    log_message "move_gridpacks" "${RED}$(getopt --test) failed in this environment${ENDCOLOR}."
    exit 1
fi

opt_short=h,p:
opt_long=help,path:
OPTS=$(getopt --options $opt_short --longoptions $opt_long -- "$@")

eval set -- "$OPTS"

while true; do
    case "$1" in
    -h | --help)
        help
        ;;
    -p | --path)
        printf "${BLUE}ARGPARSER${ENDCOLOR}: Will work in the EVNT_PATH=%s.\n" "$2"
        EVNT_PATH_GEN="$2"
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *)
        printf "${RED}ARGPARSER${ENDCOLOR}: Unexpected option: %s.\n" "$1"
        exit 1
        ;;
    esac
done

if [[ -z "$EVNT_PATH_GEN" ]]; then
    log_message "move_gridpacks" "${RED}Path to EVNTs folder (-p or --path) is required. Exiting...${ENDCOLOR}"
    exit 1
fi

# Check if GRIDPACKs exist and list them
EVNT_PATH_PROD=${EVNT_PATH_GEN/generation/production}

if find "${EVNT_PATH_PROD}" -type f -name "*.tar.gz" | grep -q .; then
    log_message "move_gridpacks" "Gridpacks ${GREEN}exist${ENDCOLOR} and are located here:"
    find "${EVNT_PATH_PROD}" -type f -name "*.tar.gz" -exec ls -lh {} +

    # Moving GRIDPACKs
    find "${EVNT_PATH_PROD}" -type f -name "*.tar.gz" | while read -r FILE; do
        DSID=$(basename "$(dirname "$FILE")")
        FILE_NAME=$(basename "$FILE")
        log_message "move_gridpacks" "Moving ${RED}${FILE_NAME}${ENDCOLOR} with DSID ${RED}${DSID}${ENDCOLOR} from production to generation folder."

        # Create output directory if it doesn't exist
        DEST_DIR="${EVNT_PATH_GEN}/${DSID}"
        if [[ ! -d "$DEST_DIR" ]]; then
            log_message "move_gridpacks" "Creating output directory: $DEST_DIR".
            mkdir -p "$DEST_DIR"
        fi

        # Move the file and check for errors
        if mv "$FILE" "$DEST_DIR"; then
            log_message "move_gridpacks" "Successfully moved ${RED}${FILE_NAME}${ENDCOLOR} to ${RED}${DEST_DIR}${ENDCOLOR}."
        else
            log_message "move_gridpacks" "Failed to move ${RED}${FILE_NAME}${ENDCOLOR} to ${RED}${DEST_DIR}${ENDCOLOR}."
            exit 1
        fi
    done

else
    log_message "move_gridpacks" "${RED}No GRIDPACK files found in ${EVNT_PATH_PROD}. Exiting...${ENDCOLOR}"
    exit 1
fi
