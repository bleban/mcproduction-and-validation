#!/bin/bash

# Logging colors
RED="\e[31m"
GREEN="\e[32m"
BLUE="\e[94m"
ENDCOLOR="\e[0m"

log_message() {
    local LOG_NAME=$1
    local LOG_MESSAGE=$2
    echo -e "${BLUE}[$(date +'%d.%m.%Y %H:%M:%S')]${ENDCOLOR} ${GREEN}${LOG_NAME}${ENDCOLOR}: ${LOG_MESSAGE}"
}
