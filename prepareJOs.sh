#!/bin/bash

help() {
    SHIFT="\t\t\t\t\t "
    echo "This script prepares joboptions from the template_<TYPE>.py file by changing the BSM particle masses."
    echo -e "Usage: prepareJOs.sh\t[ -h | --help   ] Print out this message."
    echo -e "\t\t\t[ -t | --type   ] Select type of run "'"cascades"'", "'"2hns"'", "'"4hns"'" or "'"alps"'"."
    echo -e "\t\t\t[ -m | --masses ] Input masses for which you want to prepare JOs."
    echo -e "$SHIFT Argument must be a set of desired masses passed as a string, e.g. "'"200 300 400"'"."
    echo -e "$SHIFT The DSIDs will be automatically assigned to each mass point starting from 100000."
    echo -e "$SHIFT Default array is "'"200 300 400 500 600 700 800 900 1000 1100 1200"'" for Cascades, "
    echo -e "$SHIFT "'"25:65 25:100 25:150 65:150"'" for the Heavy Neutrinos (mN:mDD), and"
    echo -e "$SHIFT "'"10 20 30 50 100"'" for the Axion-like-particles."
    echo -e "\t\t\t[ -o | --output-path   ] Output path where you want to store JOs."
    echo -e "\t\t\t[ -s | --suffix        ] Suffix to TYPE, which allows one to have multiple outputs of the same TYPE."
    echo -e "\t\t\t[ -g | --gridpack      ] Produce gridpack. Default: false."
    echo -e "\t\t\t[ -p | --save-proc-dir ] Save mg5 process directory. Default: false."
    exit 0
}

# Prepare logging
source "utils/logging.sh"

# Default values
SUFFIX=""
TYPE="2HNS"
DSID=100000
GRIDPACK=false
SAVEPROCDIR=false
PROJECT_WORKING_DIR=$(pwd)
declare -A DEFAULT_MASSES=(
    ["CASCADES"]="200 300 400 500 600 700 800 900 1000 1100 1200"
    ["2HNS"]="25:65 25:100 25:150 65:150"
    ["4HNS"]="25:65 25:100 25:150 65:150"
    ["ALPS"]="10 20 30 50 100"
)
arrMASS="${DEFAULT_MASSES[$TYPE]}"
JO_TEMPLATE_NAME="mc.MGPy8EG_A14NNPDF23LO"

# Command parsing
getopt --test >/dev/null
if [[ $? -ne 4 ]]; then
    log_message "ARGPARSER" "${RED}$(getopt --test) failed in this environment.${ENDCOLOR}"
    exit 1
fi

opt_short=h,g,p,t:,m:,o:,s:
opt_long=help,gridpack,save-proc-dir,type:,masses:,output-path:,suffix:
OPTS=$(getopt --options $opt_short --longoptions $opt_long -- "$@")

eval set -- "$OPTS"

while true; do
    case "$1" in
    -h | --help)
        help
        ;;
    -t | --type)
        TYPE=$(tr '[:lower:]' '[:upper:]' <<<"$2")
        if [[ ! "${DEFAULT_MASSES[$TYPE]}" ]]; then
            log_message "ARGPARSER" "${RED}ERROR${ENDCOLOR}: Wrong type. Please, choose from CASCADES, 2HNs, 4HNs, or ALPs. Exiting..."
            exit 2
        else
            log_message "ARGPARSER" "You chose to run in $TYPE mode."
            arrMASS="${DEFAULT_MASSES[$TYPE]}"
        fi
        shift 2
        ;;
    -m | --masses)
        log_message "ARGPARSER" "Working with masses: $2."
        arrMASS="$2"
        shift 2
        ;;
    -o | --output-path)
        log_message "ARGPARSER" "Joboptions will be stored in: $2/joboptions"
        PROJECT_WORKING_DIR="$2"
        shift 2
        ;;
    -s | --suffix)
        log_message "ARGPARSER" "Joboptions will be stored in $2 subfolder."
        SUFFIX="/$2"
        shift 2
        ;;
    -g | --gridpack)
        log_message "ARGPARSER" "Running in GRIDPACK mode."
        GRIDPACK=true
        shift
        ;;
    -p | --save-proc-dir)
        log_message "ARGPARSER" "Will keep the mg5 process directory after the generation."
        SAVEPROCDIR=true
        shift
        ;;
    --)
        shift
        break
        ;;
    *)
        log_message "ARGPARSER" "Unexpected option: ${RED}${1}${ENDCOLOR}."
        ;;
    esac
done

if [[ "$GRIDPACK" == true ]]; then
    SUFFIX="/GRIDPACKS$SUFFIX"
    log_message "ARGPARSER" "Relative path to the joboptions: joboptions/$TYPE$SUFFIX"
fi

# Check if JO_PATH already contains any directories
JO_PATH="$PROJECT_WORKING_DIR/joboptions/$TYPE$SUFFIX"
mkdir -p "$JO_PATH"
EXISTING_JOS=$(find "$JO_PATH" -mindepth 1 -maxdepth 1 -type d -name '100*' | sort -V)
if [ -n "$EXISTING_JOS" ]; then
    log_message "MCProduction" "The following DSID directories were found in joboptions/$TYPE$SUFFIX:"
    echo "$EXISTING_JOS" | awk '{print "\t- " $0}'

    echo
    while true; do
        log_message "MCProduction" "Do you want to replace all of their contents? [Y/N]"
        read -r YESNO
        case $YESNO in
        [Yy]*)
            log_message "MCProduction" "Removing contents..."
            echo "$EXISTING_JOS" | xargs rm -r
            log_message "MCProduction" "Contents removed."
            break
            ;;
        [Nn]*)
            log_message "MCProduction" "Operation canceled, JOs were neither created nor removed. Exiting..."
            exit 1
            ;;
        *)
            log_message "MCProduction" "Please answer Y or N."
            ;;
        esac
    done
else
    log_message "MCProduction" "No directories found in $JO_PATH. Proceeding..."
fi

# Main loop to prepare JOs
for MASS in $arrMASS; do

    # Logging
    if [[ "$TYPE" == "CASCADES" || "$TYPE" == "ALPS" ]]; then
        if [[ "$MASS" == *:* ]]; then
            echo -e "${RED}ERROR${ENDCOLOR}: Mass argument of wrong format, should be e.g. "'"200 300 400 500 600 700"'". Exiting..."
            exit 2
        fi
        log_message "MCProduction" "Generating JO for mass point ${RED}${MASS}${ENDCOLOR} GeV with DSID ${RED}${DSID}${ENDCOLOR}."
    elif [[ "$TYPE" == "2HNS" || "$TYPE" == "4HNS" ]]; then
        if [[ "$MASS" != *:* ]]; then
            echo -e "${RED}ERROR${ENDCOLOR}: Mass argument of wrong format, should be e.g. "'"25:100 25:62 25:142 65:142"'". Exiting..."
            exit 2
        fi
        MN=$(echo "$MASS" | cut -f1 -d:)
        MDD=$(echo "$MASS" | cut -f2 -d:)
        log_message "MCProduction" "Generating JO for mass point ${RED}${MN} : ${MDD}${ENDCOLOR} GeV (mN:mDD) with DSID ${RED}${DSID}${ENDCOLOR}."
    fi

    # Do the file manipulation
    OUTPUT="joboptions/${TYPE}${SUFFIX}/${DSID}"
    OUTPUT_FILE_PATH="${PROJECT_WORKING_DIR}/${OUTPUT}"
    mkdir -p "$OUTPUT_FILE_PATH"

    if [[ "$TYPE" == "CASCADES" ]]; then
        OUTPUT_FILE_NAME="${JO_TEMPLATE_NAME}_TypeIISeesawCascades_M${MASS}.py"
    elif [[ "$TYPE" == "2HNS" ]]; then
        OUTPUT_FILE_NAME="${JO_TEMPLATE_NAME}_2HNS_N${MN}_DD${MDD}.py"
    elif [[ "$TYPE" == "4HNS" ]]; then
        OUTPUT_FILE_NAME="${JO_TEMPLATE_NAME}_4HNS_N${MN}_DD${MDD}.py"
    elif [[ "$TYPE" == "ALPS" ]]; then
        OUTPUT_FILE_NAME="${JO_TEMPLATE_NAME}_gammagamma2ALP2dimuon_M${MASS}.py"
    fi

    # Copy control file to the first DSID and create a symlink for the rest
    if [[ "$DSID" -eq 100000 ]]; then
        cp "templates/MadGraphControl_${TYPE}.py" "$OUTPUT_FILE_PATH"

        # Keep the process dir
        if [[ "$SAVEPROCDIR" == true ]]; then
            sed -i "2 s/False/True/g" "${OUTPUT_FILE_PATH}/MadGraphControl_${TYPE}.py"
        fi
        # Run in GRIDPACK mode
        if [[ "$GRIDPACK" == true ]]; then
            sed -i "3 s/False/True/g" "${OUTPUT_FILE_PATH}/MadGraphControl_${TYPE}.py"
        fi

    else
        ln -s "${OUTPUT_FILE_PATH/${DSID}/100000}/MadGraphControl_${TYPE}.py" "$OUTPUT_FILE_PATH"
    fi
    echo "include('MadGraphControl_${TYPE}.py')" >"${OUTPUT_FILE_PATH}/${OUTPUT_FILE_NAME}"

    DSID=$((DSID + 1))
done

# Prepare config.sh file with file input paths
{
    echo "# Mandatory configuration"
    echo "export TYPE='""$TYPE""'"
    echo "export JO_PATH='""$PROJECT_WORKING_DIR/joboptions/$TYPE$SUFFIX""'"
    echo "export EVNT_PATH='""$PROJECT_WORKING_DIR/EVNTs/$TYPE$SUFFIX""'"
    echo "export DAOD_PATH='""$PROJECT_WORKING_DIR/DAOD_TRUTHs/$TYPE$SUFFIX""'"
} >config.sh

echo
log_message "MCProduction" "Script execution completed successfully."
